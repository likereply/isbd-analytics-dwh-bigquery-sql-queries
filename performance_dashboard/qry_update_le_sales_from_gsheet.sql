SELECT
  year,
  month,
  bank,
  segment,
  channel,
  isbd_performance_dialogue.udf_int64(total_sales_units) as total_sales_units,
  isbd_performance_dialogue.udf_float64(total_sales_volume) as total_sales_volume,
  isbd_performance_dialogue.udf_int64(deposits_units) as deposits_units,
  isbd_performance_dialogue.udf_float64(deposits_volume) as deposits_volume,
  isbd_performance_dialogue.udf_int64(creditline_units) as creditline_units,
  isbd_performance_dialogue.udf_float64(creditline_volume) as creditline_volume,
  isbd_performance_dialogue.udf_int64(guarantees_units) as guarantees_units,
  isbd_performance_dialogue.udf_float64(guarantees_volumes) as guarantees_volumes,
  isbd_performance_dialogue.udf_int64(e_invoicing_units) as e_invoicing_units,
  isbd_performance_dialogue.udf_int64(open_banking_units) as open_banking_units,
  isbd_performance_dialogue.udf_int64(digi4biz_no) as digi4biz_no,
  isbd_performance_dialogue.udf_int64(old_ib_no) as old_ib_no,
  isbd_performance_dialogue.udf_int64(old_mb_no) as old_mb_no,
  DATE(CAST(year AS INT64),CAST(month AS INT64),1) AS date
FROM `isbd-analytics-global-dwh.isbd_performance_dialogue.le_sales_gsheet`
where year is not null