SELECT
  year,
  month,
  bank,
  segment,
  channel,
  isbd_performance_dialogue.udf_int64(customers_with_digital_contract) as customers_with_digital_contract,
  isbd_performance_dialogue.udf_int64(active_customers) as active_customers,
  isbd_performance_dialogue.udf_int64(customers_with_transactions) as customers_with_transactions,
  isbd_performance_dialogue.udf_int64( lost_digital_customers ) as lost_digital_customers,
  isbd_performance_dialogue.udf_int64( new_digital_customers ) as new_digital_customers,
  isbd_performance_dialogue.udf_int64( net_growth_digital_customers ) as net_growth_digital_customers,
  isbd_performance_dialogue.udf_float64( digital_subscription_fees ) as digital_subscription_fees,
  DATE(CAST(year AS INT64),CAST(month AS INT64),1) AS date
FROM
  `isbd-analytics-global-dwh.isbd_performance_dialogue.customer_gsheet`
WHERE
  year IS NOT NULL;