SELECT
  year,
  month,
  bank,
  segment,
  channel,
  isbd_performance_dialogue.udf_int64(total_transactions) as total_transactions,
  isbd_performance_dialogue.udf_float64(total_transactions_volume) as total_transactions_volume,
  isbd_performance_dialogue.udf_int64(transfer_to_myself) as transfer_to_myself,
  isbd_performance_dialogue.udf_float64( transfer_to_myself_volume ) as transfer_to_myself_volume,
  isbd_performance_dialogue.udf_int64( currency_conversions ) as currency_conversions,
  isbd_performance_dialogue.udf_float64( currency_conversions_volume ) as currency_conversions_volume,
  isbd_performance_dialogue.udf_int64( payments ) as payments,
  isbd_performance_dialogue.udf_float64( payments_volume ) as payments_volume,
  isbd_performance_dialogue.udf_int64( fx_payments ) as fx_payments,
  isbd_performance_dialogue.udf_float64( fx_payments_volume ) as fx_payments_volume,
  isbd_performance_dialogue.udf_int64( deposits ) as deposits,
  isbd_performance_dialogue.udf_float64( deposits_volume ) as deposits_volume,
  isbd_performance_dialogue.udf_int64( withdrawals ) as withdrawals,
  isbd_performance_dialogue.udf_float64( withdrawals_volume ) as withdrawals_volume,
  isbd_performance_dialogue.udf_float64( transaction_fees ) as transaction_fees,
  DATE(CAST(year AS INT64),CAST(month AS INT64),1) AS date
FROM
  `isbd-analytics-global-dwh.isbd_performance_dialogue.transaction_gsheet`
WHERE
  year IS NOT NULL;