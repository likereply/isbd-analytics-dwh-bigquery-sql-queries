SELECT
  year,
  quarter,
  month,
  bank,
  isbd_performance_dialogue.udf_int64(personal_loans_units) as personal_loans_units,
  isbd_performance_dialogue.udf_float64(personal_loans_volume) as personal_loans_volume,
  isbd_performance_dialogue.udf_int64(cpi_units) as cpi_units,
  isbd_performance_dialogue.udf_float64(overdraft) as overdraft,
  isbd_performance_dialogue.udf_float64(deposit) as deposit,
  isbd_performance_dialogue.udf_float64(mutual_fund) as mutual_fund,
  isbd_performance_dialogue.udf_float64(credit_card) as credit_card,
  isbd_performance_dialogue.udf_float64(remote_offer) as remote_offer,
  DATE(CAST(year AS INT64),CAST(month AS INT64),1) AS date
FROM
  `isbd-analytics-global-dwh.isbd_performance_dialogue.idv_sales_target_gsheet`