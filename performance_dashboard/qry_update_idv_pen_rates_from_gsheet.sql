SELECT
  year,
  month,
  bank,
  customer,
  isbd_performance_dialogue.udf_float64(current_accounts) as current_accounts,
  isbd_performance_dialogue.udf_float64(debit_cards) as debit_cards,
  isbd_performance_dialogue.udf_float64(advisory_service) as advisory_service,
  isbd_performance_dialogue.udf_float64(credit_cards) as credit_cards,
  isbd_performance_dialogue.udf_float64(personal_loans) as personal_loans,
  isbd_performance_dialogue.udf_float64(cpi) as cpi,
  isbd_performance_dialogue.udf_float64(overdrafts) as overdrafts,
  isbd_performance_dialogue.udf_float64(mortgages) as mortgages,
  isbd_performance_dialogue.udf_float64(mutual_funds) as mutual_funds,
  isbd_performance_dialogue.udf_float64(deposits) as deposits,
  isbd_performance_dialogue.udf_float64(simplesurances) as simplesurances,
  isbd_performance_dialogue.udf_float64(salary_pension) as salary_pension,
  isbd_performance_dialogue.udf_float64(digital_channels) as digital_channels,
  DATE(CAST(year AS INT64),CAST(month AS INT64),1) AS date
FROM
  `isbd-analytics-global-dwh.isbd_performance_dialogue.idv_pen_rates_gsheet`