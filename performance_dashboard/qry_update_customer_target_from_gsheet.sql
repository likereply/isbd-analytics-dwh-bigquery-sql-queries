SELECT
  year,
  quarter,
  month,
  bank,
  isbd_performance_dialogue.udf_int64(customers_with_digital_contract) as customers_with_digital_contract,
  isbd_performance_dialogue.udf_int64(customers_with_login) as customers_with_login,
  isbd_performance_dialogue.udf_int64(customers_with_transactions) as customers_with_transactions,
  DATE(CAST(year AS INT64),CAST(month AS INT64),1) AS date
FROM
  `isbd-analytics-global-dwh.isbd_performance_dialogue.customer_target_gsheet`