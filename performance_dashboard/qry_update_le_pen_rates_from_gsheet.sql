SELECT
  year,
  month,
  bank,
  customer,
  isbd_performance_dialogue.udf_float64(deposits) as deposits,
  isbd_performance_dialogue.udf_float64(creditline) as creditline,
  isbd_performance_dialogue.udf_float64(guarantees) as guarantees,
  isbd_performance_dialogue.udf_float64(e_invoicing) as e_invoicing,
  isbd_performance_dialogue.udf_float64(open_banking) as open_banking,
  isbd_performance_dialogue.udf_float64(digital_channels) as digital_channels,
  DATE(CAST(year AS INT64),CAST(month AS INT64),1) AS date
FROM
  `isbd-analytics-global-dwh.isbd_performance_dialogue.le_pen_rates_gsheet`