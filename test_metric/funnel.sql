SELECT
function,
step,
count(distinct user_pseudo_id) as n_users
FROM `digical-184607.analytics_163239471.events_*`
left join `isbd-analytics-global-dwh.isbd_global_ga_PROD.funnel` on CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING )=step
where PARSE_DATE('%Y%m%d', event_date) <="2021-01-10" and  stream_id in ("1882516406", "1411574163", "2201252152") and function in ("withpay",
"transfer to myself",
"transfer to someone",
"foreign payment",
"saving explorer",
"saving explorer guided",
"withsave",
"request loan",
"investment",
"registration") and
--CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING )="savings"
 (REGEXP_CONTAINS(CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ), r"\binternetbanking.pbz.hr\b") or platform in ("ANDROID", "IOS"))
group by 1,2
order by 1, 3 DESC