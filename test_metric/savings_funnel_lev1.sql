-- COUNT FIREBASE_SCREEN - ONLY APP
SELECT
firebase_screen,
count(firebase_screen)
from
`isbd-analytics-global-dwh.isbd_global_ga_PROD.executive_level_1`
--left join `isbd-analytics-global-dwh.isbd_global_ga_PROD.funnel` on firebase_screen=step
where firebase_screen in ("start_saving_explorer", "detail_saving_explorer","config_saving_explorer","summary_saving_explorer","contract_saving_explorer","transaction_successful_saving_explorer")
and platform in ("IOS", "ANDROID") and event_date < "2021-01-18"
group by 1
order by 2 desc

-- COUNT USER_PSEUDO_ID - ONLY APP
SELECT
firebase_screen,
count(distinct user_pseudo_id)
from
`isbd-analytics-global-dwh.isbd_global_ga_PROD.executive_level_1`
--left join `isbd-analytics-global-dwh.isbd_global_ga_PROD.funnel` on firebase_screen=step
where firebase_screen in ("start_saving_explorer", "detail_saving_explorer","config_saving_explorer","summary_saving_explorer","contract_saving_explorer","transaction_successful_saving_explorer")
and platform in ("IOS", "ANDROID") and event_date < "2021-01-18"
group by 1
order by 2 desc