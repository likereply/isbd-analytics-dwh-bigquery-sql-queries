SELECT
COUNT(distinct user_pseudo_id)
FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.pbz_lev0_test_20210104_20210110`
where PARSE_DATE('%Y%m%d', event_date) ="2021-01-10" and  stream_id in ("1882516406", "1411574163", "2201252152") and  event_name in ("fund_purchase", "fund_sell")
and (REGEXP_CONTAINS(CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ), r"\binternetbanking.pbz.hr\b") or platform in ("ANDROID", "IOS"))