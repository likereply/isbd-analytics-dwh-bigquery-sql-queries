SELECT
AVG(CAST(SUBSTR(saving_tenor, 1, 2) as INT64)*365 + CAST(SUBSTR(saving_tenor, 3, 2) as INT64)*30 + CAST(SUBSTR(saving_tenor, 5, 2) as INT64)) as days_saving_tenor
from
(SELECT
(CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="saving_tenor") as string )) as saving_tenor
FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.pbz_lev0_test_20210104_20210110`
where PARSE_DATE('%Y%m%d', event_date) ="2021-01-10" and  stream_id in ("1882516406", "1411574163", "2201252152") and  event_name="purchase_saving"
and (REGEXP_CONTAINS(CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ), r"\binternetbanking.pbz.hr\b") or platform in ("ANDROID", "IOS")))