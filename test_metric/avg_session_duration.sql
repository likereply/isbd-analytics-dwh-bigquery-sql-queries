with
prep as (
select
  user_pseudo_id,
  event_date as date,
  platform,
  CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING) as firebase_screen_class,
  timestamp_micros(event_timestamp) as event_timestamp,
  event_name,
  lead(event_name) over (partition by user_pseudo_id order by event_timestamp) as next_event,
  timestamp_diff( lead(timestamp_micros(event_timestamp)) over (partition by user_pseudo_id order by event_timestamp), timestamp_micros(event_timestamp), second) as time_to_next_event
from
  `digical-184607.analytics_163239471.events_*`   -- change this to your table
-- no WHERE clause, every event counts
)
,sessionize as(
  select
    user_pseudo_id,
    event_timestamp,
    date,
    platform,
    firebase_screen_class,
    event_name,
    1 + countif(time_to_next_event is null or time_to_next_event > 300) over
        (partition by user_pseudo_id order by event_timestamp asc
         rows between unbounded preceding and 1 preceding) as session_number,
    time_to_next_event
  from prep
)
-- summarize per session
select
  user_pseudo_id ||'-'|| session_number as session_id,
  PARSE_DATE('%Y%m%d', date) as date,
  platform,
  firebase_screen_class,
  --min(event_timestamp) as first_event,
  --max(event_timestamp) as last_event,
  timestamp_diff(max(event_timestamp), min(event_timestamp), second) as session_duration
from sessionize
group by 1,2,3,4
order by 1