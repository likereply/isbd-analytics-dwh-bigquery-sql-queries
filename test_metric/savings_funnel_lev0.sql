-- COUNT FIREBASE_SCREEN - ONLY APP
SELECT
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
COUNT(CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING )) as counter,
FROM `digical-184607.analytics_163239471.events_*`
where PARSE_DATE('%Y%m%d', event_date) < "2021-01-18" and stream_id in ("1882516406", "1411574163", "2201252152")
and CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) in ("start_saving_explorer", "detail_saving_explorer","config_saving_explorer","summary_saving_explorer","contract_saving_explorer","transaction_successful_saving_explorer")
and platform in ("IOS", "ANDROID")
group by 1
order by 2 desc

-- COUNT USER_PSEUDO_ID - ANLY APP
SELECT
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
--COUNT(CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="user_pseudo_id") as STRING )) as counter,
COUNT(distinct user_pseudo_id)
FROM `digical-184607.analytics_163239471.events_*`
where PARSE_DATE('%Y%m%d', event_date) < "2021-01-18" and stream_id in ("1882516406", "1411574163", "2201252152")
and CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) in ("start_saving_explorer", "detail_saving_explorer","config_saving_explorer","summary_saving_explorer","contract_saving_explorer","transaction_successful_saving_explorer")
and platform in ("IOS", "ANDROID")
group by 1
order by 2 desc
