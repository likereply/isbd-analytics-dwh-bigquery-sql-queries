SELECT
TIMESTAMP_MICROS(event_timestamp),
user_pseudo_id,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="ga_session_number") as STRING ) as ga_session_number,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="ga_session_id") as STRING ) as ga_session_id,
FROM `digical-184607.analytics_163239471.events_*`
where stream_id in ("1882516406", "1411574163", "2201252152") and
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="ga_session_id") as STRING ) = "1610121518"
 and user_pseudo_id = "25e6c267166d90048547904ec771d5d5"
order by 1,2 desc