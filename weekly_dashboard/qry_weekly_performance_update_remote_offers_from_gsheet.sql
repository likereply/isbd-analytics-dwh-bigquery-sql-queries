SELECT
  year,
  CAST(week AS INT64) as week,
  segment,
  channel,
  bank,
  isbd_performance_dialogue.udf_int64(total_remote_offer) as total_remote_offer,
  isbd_performance_dialogue.udf_int64(isps_account) as isps_account,
  isbd_performance_dialogue.udf_int64(isps_generic) as isps_generic,
  isbd_performance_dialogue.udf_int64(isps_loan) as isps_loan,
  isbd_performance_dialogue.udf_int64(isps_saving) as isps_saving,
  isbd_performance_dialogue.udf_int64(isps_cards) as isps_cards,
  isbd_performance_dialogue.udf_int64(isps_insurance) as isps_insurance,
  isbd_performance_dialogue.udf_int64(isps_investments) as isps_investments,
  isbd_performance_dialogue.udf_int64(pbz_changing_limits) as pbz_changing_limits,
  isbd_performance_dialogue.udf_int64(pbz_closing) as pbz_closing,
  isbd_performance_dialogue.udf_int64(pbz_moratorium) as pbz_moratorium,
  isbd_performance_dialogue.udf_int64(pbz_new_subscription) as pbz_new_subscription,
  isbd_performance_dialogue.udf_int64(pbz_changing_payment) as pbz_changing_payment,
  isbd_performance_dialogue.udf_int64(pbz_data_update) as pbz_data_update,
  isbd_performance_dialogue.udf_int64(pbz_decreasing_overdraft) as pbz_decreasing_overdraft,
  isbd_performance_dialogue.udf_int64(vub_current_accounts) as vub_current_accounts,
  isbd_performance_dialogue.udf_int64(vub_mortgages) as vub_mortgages,
  isbd_performance_dialogue.udf_int64(vub_mutual_funds) as vub_mutual_funds,
  isbd_performance_dialogue.udf_int64(vub_credit_cards) as vub_credit_cards,
  isbd_performance_dialogue.udf_int64(vub_consumer_loans) as vub_consumer_loans,
  isbd_performance_dialogue.udf_int64(vub_nonstop_banking) as vub_nonstop_banking,
  isbd_performance_dialogue.udf_int64(vub_deposits) as vub_deposits,
  isbd_performance_dialogue.udf_int64(vub_life_insurance) as vub_life_insurance,
  isbd_performance_dialogue.udf_int64(ispa_current_account) as ispa_current_account,
  isbd_performance_dialogue.udf_int64(ispa_debit_cards) as ispa_debit_cards,
  isbd_performance_dialogue.udf_int64(ispa_personal_loan) as ispa_personal_loan,
  DATE_ADD(DATE (concat(year,"-01-04")), INTERVAL CAST(week AS INT64)*7-7 DAY) as date
FROM
  `isbd-analytics-global-dwh.isbd_performance_weekly.remote_offers_gsheet`
where year is not null