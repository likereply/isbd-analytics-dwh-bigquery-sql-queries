SELECT
  year,
  CAST(week AS INT64) as week,
  segment,
  channel,
  bank,
  isbd_performance_dialogue.udf_int64(deposits_units) as deposits_units,
  isbd_performance_dialogue.udf_float64(deposits_volume) as deposits_volume,
  isbd_performance_dialogue.udf_int64(creditline_units) as creditline_units,
  isbd_performance_dialogue.udf_float64(creditline_volume) as creditline_volume,
  isbd_performance_dialogue.udf_int64(guarantees_units) as guarantees_units,
  isbd_performance_dialogue.udf_float64(guarantees_volume) as guarantees_volume,
  isbd_performance_dialogue.udf_int64(einvoicing_units) as einvoicing_units,
  isbd_performance_dialogue.udf_int64(open_banking_units) as open_banking_units,
  DATE_ADD(DATE (concat(year,"-01-04")), INTERVAL CAST(week AS INT64)*7-7 DAY) as date
FROM
  `isbd-analytics-global-dwh.isbd_performance_weekly.le_sales_gsheet`
where year is not null