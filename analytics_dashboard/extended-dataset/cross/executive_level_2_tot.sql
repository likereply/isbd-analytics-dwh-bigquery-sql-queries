SELECT
*,
ifnull(income_fees_loans, 0) + ifnull(income_interest_loans, 0) as total_income_loans,
ifnull(rejected_income_fees_loans, 0) + ifnull(rejected_income_interest_loans,0) as total_rejected_income_loans,
ifnull(income_fees_savings, 0) + ifnull(income_interest_savings,0) as total_income_savings,
ifnull(ifnull(income_payments,0) + ifnull(income_gsm_voucher,0) + ifnull(income_cards,0) + ifnull(income_investments,0) + ifnull(income_fees_savings,0) + ifnull(income_interest_savings,0) + ifnull(income_fees_loans,0) - ifnull(rejected_income_fees_loans,0) + ifnull(income_interest_loans,0) - ifnull(rejected_income_interest_loans,0),0) as total_digital_income -- + income_interest_savings + income insurance
FROM
(SELECT
*,
ifnull(value,0) * ifnull(conversion,0) as value_eur,
loan_interest - ftp as markup,
ftp - saving_interest_rate as markdown, -- manca saving_interest
ifnull((CASE WHEN firebase_screen_class="payments" and event_name="transaction" and event_name!="saved_transaction" THEN ifnull(fee, 0) * ifnull(transactions, 0) END),0) as income_payments,
ifnull((CASE WHEN firebase_screen_class="gsm_voucher" and event_name="transaction" THEN ifnull(fee, 0) * ifnull(transactions, 0) END),0) as income_gsm_voucher, -- questa in realtà va dentro payments
ifnull((CASE WHEN firebase_screen_class="cards" and event_name="transaction" THEN ifnull(fee, 0) * ifnull(transactions, 0) END), 0) as income_cards, -- card_payoff. c'è payoff, ma manca  virtual_card
ifnull((CASE WHEN firebase_screen_class="investment" and (event_name="fund_purchase" or event_name="fund_sell") THEN ifnull(fee,0) * ifnull(transactions,0) END),0) as income_investments, -- inserito anche fund sell ma è sbagliato per mancanza di ID
ifnull((CASE WHEN firebase_screen_class="savings" and (event_name="transaction" or event_name="purchase_saving") THEN ifnull(fee,0) * ifnull(savings,0) END),0) as income_fees_savings,
ifnull((CASE WHEN firebase_screen_class="savings" and (event_name="transaction" or event_name="purchase_saving") THEN (ifnull(value,0) * ifnull(conversion,0)) * (ftp - saving_interest_rate) END),0) as income_interest_savings, -- da aggiornare poi con i dati veri dell'interest
ifnull((CASE WHEN firebase_screen_class="loans" and event_name="purchase_loan" THEN ifnull(fee,0) * ifnull(loans,0) END),0) as income_fees_loans,
ifnull((CASE WHEN firebase_screen_class="loans" and event_name="loan_rejected" THEN fee * loans END),0) as rejected_income_fees_loans,
-- income insurance: insurances fee * insurances
ifnull((CASE WHEN firebase_screen_class="loans" and event_name="purchase_loan" THEN (ifnull(value,0) * ifnull(conversion,0)) * (loan_interest - ftp) END),0) as income_interest_loans,
ifnull((CASE WHEN firebase_screen_class="loans" and event_name="loan_rejected" THEN (ifnull(value,0) * ifnull(conversion,0)) * (loan_interest - ftp) END),0) as rejected_income_interest_loans,
(CASE
WHEN firebase_screen_class="loans" and event_name="purchase_loan" and (ifnull(value,0) * ifnull(conversion,0))>0 and (ifnull(value,0) * ifnull(conversion,0))<=2000  THEN "1-2000"
WHEN firebase_screen_class="loans" and event_name="purchase_loan" and (ifnull(value,0) * ifnull(conversion,0))>2000 and (ifnull(value,0) * ifnull(conversion,0))<=6000  THEN "2000-6000"
WHEN firebase_screen_class="loans" and event_name="purchase_loan" and (ifnull(value,0) * ifnull(conversion,0))>6000 and (ifnull(value,0) * ifnull(conversion,0))<=12000  THEN "6000-12000"
WHEN firebase_screen_class="loans" and event_name="purchase_loan" and (ifnull(value,0) * ifnull(conversion,0))>12000  THEN ">12000"
else null
END) as loans_cluster_amount,
from
`isbd-analytics-global-dwh.isbd_global_ga_PROD.executive_level_1`)
left join `isbd-analytics-global-dwh.isbd_global_ga_PROD.funnel` on firebase_screen=step
