SELECT
ga.*,
bm.* EXCEPT(bank_name, year),
cc.*,
target.* EXCEPT(year, bank, product, target_id),
saving_ir.* EXCEPT(year, bank, saving_name, id_saving_interest)
FROM
(SELECT
CONCAT(year, "ISPS", firebase_screen_class) as join_key_bm,
CONCAT(year, "ISPS", saving_name) as join_key_saving,
*
FROM
(SELECT
"ISPS" as bank_name,
--CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)), "PBZ", firebase_screen_class) as join_key_bm,
PARSE_DATE('%Y%m%d', event_date)as event_date,
EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)) as year,
stream_id,
platform,
event_name,
name as traffic_source_name,
traffic_medium as traffic_source_medium,
source as traffic_source_source,
campaign,
page_referrer,
medium,

user_id,
user_pseudo_id,

country,
region,
city,
category as device_category,
operating_system,
browser,

(case when platform="WEB" then
REGEXP_EXTRACT(
(
SELECT string_agg(url.level_1, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where  platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
), r'([^;]+);?$')
else
 firebase_screen_class
end
) as firebase_screen_class,

(case when platform="WEB" then
REGEXP_EXTRACT(
(
SELECT string_agg(url.level_2, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
), r'([^;]+);?$') else
 firebase_screen
end
) as  firebase_screen,
REPLACE(page_location, "react/", "") as page_location,
page_title,
section,
section_tab,

transaction_type,

fund_name,
area,
risk,
horizon,
sector,
starting_point,

insurance_company,
insurance_duration,
insurance_name,
Insurance_price,
insurance_product_brand,
insurance_product_type,
insured_amount,

loan_insurance,
loan_interest,
loan_name,
loan_tenor,
original_currency,

saving_name,
saving_renewal_auto,
saving_tenor,
CAST(SUBSTR(saving_tenor, 1, 2) as INT64)*365 + CAST(SUBSTR(saving_tenor, 3, 2) as INT64)*30 + CAST(SUBSTR(saving_tenor, 5, 2) as INT64) as days_saving_tenor,

currency,

SUM(value) as value,
COUNT(distinct loan_purchase_id) as loans,
--COUNT(distinct saving_purchase_id) as savings,
CASE when event_name="purchase_saving" then count(event_name) end as savings,
COUNT(distinct transaction_id) as transactions,


from
(SELECT
event_timestamp,
event_date,
stream_id,
platform,
event_name,
traffic_source.name,
traffic_source.medium as traffic_medium,
traffic_source.source,
user_id,
user_pseudo_id,
geo.country,
geo.region,
geo.city,
device.category,
device.operating_system,
device.web_info.browser as browser,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="area") as STRING ) as area,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="campaign") as STRING ) as campaign,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="currency") as STRING ) as currency,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING ) as firebase_screen_class,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="fund_name") as STRING ) as fund_name,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="horizon") as STRING ) as horizon,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="insurance_company") as STRING ) as insurance_company,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="insurance_duration") as STRING ) as insurance_duration,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="insurance_name") as STRING ) as insurance_name,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="Insurance_price") as FLOAT64 ) as Insurance_price,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="insurance_product_brand") as STRING ) as insurance_product_brand,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="insurance_product_type") as STRING ) as insurance_product_type,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="insured_amount") as FLOAT64 ) as insured_amount,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="loan_insurance") as STRING ) as loan_insurance,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="loan_interest") as FLOAT64 ) as loan_interest,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="loan_name") as STRING ) as loan_name,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="loan_purchase_id") as STRING ) as loan_purchase_id,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="loan_tenor") as INT64 ) as loan_tenor,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="original_currency") as STRING ) as original_currency,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ) as page_location,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_referrer") as STRING ) as page_referrer,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_title") as STRING ) as page_title,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="risk") as FLOAT64 ) as risk,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="saving_name") as STRING ) as saving_name,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="saving_purchase_id") as STRING ) as saving_purchase_id,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="saving_renewal_auto") as STRING ) as saving_renewal_auto,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="saving_tenor") as STRING ) as saving_tenor,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="section") as STRING ) as section,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="section_tab") as STRING ) as section_tab,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="sector") as STRING ) as sector,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="starting_point") as STRING ) as starting_point,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="transaction_id") as STRING ) as transaction_id,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="transaction_type") as STRING ) as transaction_type,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="medium") as STRING ) as medium,
CAST((SELECT REPLACE(REPLACE(REPLACE(COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)), ",", "."),"null", "0"), "[object Object]", "0") FROM UNNEST(event_params) WHERE key="value") as FLOAT64) as value
FROM
`api-project-808669006573.analytics_204727721.events*`
where PARSE_DATE('%Y%m%d', event_date) <= DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY) and stream_id in ("2202527496", "1882536869", "1495499099")
)
group by
--CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)), "PBZ", firebase_screen_class),
event_date,
EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),
stream_id,
platform,
event_name,
name,
traffic_medium,
source,
campaign,
page_referrer,
medium,

user_id,
user_pseudo_id,

country,
region,
city,
category,
operating_system,
browser,

firebase_screen_class,
firebase_screen,
REPLACE(page_location, "react/", ""),
page_title,
section,
section_tab,

transaction_type,

fund_name,
area,
risk,
horizon,
sector,
starting_point,

insurance_company,
insurance_duration,
insurance_name,
Insurance_price,
insurance_product_brand,
insurance_product_type,
insured_amount,

loan_insurance,
loan_interest,
loan_name,
loan_tenor,
original_currency,

saving_name,
saving_renewal_auto,
saving_tenor,

currency)) as ga
LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.isbd_business_metrics` as bm on join_key_bm=join_key_ga
LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.currency_converter` as cc on currency=iso_code
LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.target` as target on target_id=join_key_bm
LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.saving_interest_rate` as saving_ir on id_saving_interest=join_key_saving

