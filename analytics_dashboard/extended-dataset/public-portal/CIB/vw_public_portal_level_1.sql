SELECT
  "CIB" as bank_name,
  PARSE_DATE("%Y%m%d", date) as e_date,
  fullVisitorId,
  concat(fullVisitorId,visitId) as sessionId,
  hits.page.pagePath as page_path,
  hits.type as hits_type,
  (CASE WHEN REGEXP_CONTAINS(hits.page.pagePath, "/onboarding/lightsignup") THEN 1 ELSE 0 END) as on_boarding_start,
  (CASE WHEN REGEXP_CONTAINS(hits.page.pagePath, "--------------") THEN 1 ELSE 0 END) as product_proposition_sessions, #TO CHANGE ACCORDINGLY TO THE SPECIFIC BANK
  (CASE WHEN REGEXP_CONTAINS(hits.eventInfo.eventAction, "added to cart PP") THEN 1 ELSE 0 END) as add_to_cart,
  --(CASE WHEN hits.eventInfo.eventAction like "%lightsignup%" THEN 1 ELSE 0 END) as light_signup,
  (CASE WHEN REGEXP_CONTAINS(hits.page.pagePath, "/onboarding/lightsignup") THEN 1 ELSE 0 END) as light_signup,
  (CASE WHEN REGEXP_CONTAINS(hits.page.pagePath, "/onboarding/welcome") THEN 1 ELSE 0 END) as on_boarding_completion,
  (CASE WHEN hits.eventInfo.eventCategory = "contactus" THEN 1 ELSE 0 END) as contact_us,
  (CASE WHEN hits.eventInfo.eventCategory = "appointments" THEN 1 ELSE 0 END) as appointments,
  (CASE WHEN hits.eventInfo.eventCategory = "call me back" THEN 1 ELSE 0 END) as call_me_back,
  (CASE WHEN REGEXP_CONTAINS(hits.page.pagePath, "/leave-message/branch") THEN 1 ELSE 0 END) as branch_locator, #TO CHANGE ACCORDINGLY TO THE SPECIFIC BANK
  (
    CASE  WHEN REGEXP_CONTAINS(hits.page.pagePath,"/onboarding/lightsignup") THEN "1.Ligt Signup"
          WHEN REGEXP_CONTAINS(hits.page.pagePath,"/onboarding/usermessages/verifyemail") THEN "2.Verify Email"
          WHEN REGEXP_CONTAINS(hits.page.pagePath,"/onboarding/documentsupload") THEN "3.Documents Upload"
          WHEN REGEXP_CONTAINS(hits.page.pagePath,"/onboarding/documentsupload/uploadphoto|/onboarding/documentsupload/mobileupload") THEN "4.Upload via Phone / PC"
          WHEN REGEXP_CONTAINS(hits.page.pagePath,"/onboarding/personaldata/personal") THEN "5.1.Personal Data - Basic Info"
          WHEN REGEXP_CONTAINS(hits.page.pagePath,"/onboarding/personaldata/address") THEN "5.2.Personal Data - Address"
          WHEN REGEXP_CONTAINS(hits.page.pagePath,"/onboarding/personaldata/contacts") THEN "5.3.Personal Data - Contacts"
          WHEN REGEXP_CONTAINS(hits.page.pagePath,"/onboarding/personaldata/employment") THEN "5.4.Personal Data - Employment"
          WHEN REGEXP_CONTAINS(hits.page.pagePath,"/onboarding/consent") THEN "6.Legal Info 1"
          WHEN REGEXP_CONTAINS(hits.page.pagePath,"/onboarding/consents") THEN "7.Legal Info 2"
          WHEN REGEXP_CONTAINS(hits.page.pagePath,"/onboarding/videochat/consent") THEN "8.Video Chat Consent"
          WHEN REGEXP_CONTAINS(hits.page.pagePath,"/onboarding/videochat/check") THEN "9.Video Chat Check"
          WHEN REGEXP_CONTAINS(hits.page.pagePath,"/onboarding/videochat/verify") THEN "10.Video Chat Verify"
          WHEN REGEXP_CONTAINS(hits.page.pagePath,"/onboarding/longtimeservice") THEN "11.LongTimeService"
          WHEN REGEXP_CONTAINS(hits.page.pagePath,"/onboarding/certificate") THEN "12.Certificate"
          WHEN REGEXP_CONTAINS(hits.page.pagePath,"/onboarding/certificate/pdf") THEN "13.Certificate PDF"
          WHEN REGEXP_CONTAINS(hits.page.pagePath,"/onboarding/choosebranch") THEN "14.Choose Branch"
          WHEN REGEXP_CONTAINS(hits.page.pagePath,"/onboarding/choosebranch/detail") THEN "15.Choose Branch Detail"
          WHEN REGEXP_CONTAINS(hits.page.pagePath,"/onboarding/pennytransfer") THEN "16.Penny Transfer"
          WHEN REGEXP_CONTAINS(hits.page.pagePath,"/onboarding/welcome") THEN "17.Thank You Page"
          ELSE "Other"
          END) as funnel_step,
  (CASE WHEN hits.page.pagePath like "%/onboarding/%" THEN "Funnel started" ELSE "Funnel not started" END) as on_boarding_funnel_status,
  (SELECT value from hits.customDimensions where index=9) as product_family,
  (SELECT value from hits.customDimensions where index=10) as product_name,
  concat(trafficSource.source, ' / ', trafficSource.medium) AS source_medium,
  trafficSource.source,
  trafficSource.medium,
  trafficSource.keyword,
  trafficSource.campaign,
  (CASE
      WHEN ((trafficSource.source="direct" AND trafficSource.medium="(not set)") OR trafficSource.medium="(none)") THEN "Direct"
      WHEN trafficSource.medium="organic" THEN "Organic Search"
      WHEN REGEXP_CONTAINS(trafficSource.source,"^(.*social.*|.*social-network.*|.*social-media.*|sm|.*social network.*|.*social media.*)$") THEN "Social"
      WHEN trafficSource.medium="email" THEN "Email"
      WHEN trafficSource.medium="affiliate" THEN "Affiliates"
      WHEN trafficSource.medium="referral" THEN "Referral"
      WHEN (REGEXP_CONTAINS(trafficSource.medium,"^(.*cpc.*|.*ppc.*|.*paidsearch.*)$ "))  THEN "Paid Search"
      WHEN REGEXP_CONTAINS(trafficSource.medium,"^(.*cpv.*|.*cpa.*|.*cpp.*|.*content-text.*)$") THEN "Other Advertising"
      WHEN (REGEXP_CONTAINS(trafficSource.medium,"^(.*display.*|.*cpm.*|.*banner.*)$")) THEN "Display"
      WHEN (REGEXP_CONTAINS(trafficSource.medium,"^(.*native.*)$")) THEN "Native"
      WHEN NOT(REGEXP_CONTAINS(trafficSource.source,"^(.*social.*|.*social-network.*|.*social-media.*|sm|.*social network.*|.*social media.*)$")) AND (REGEXP_CONTAINS(trafficSource.medium,"^(.*video.*)$")) THEN "Video"
      ELSE "(Other)"
  END) as channel,
  geoNetwork.country,
  geoNetwork.region,
  geoNetwork.city,
  SUM(IF(hits.type = 'PAGE',1,0)) as page_views,
  count(distinct case when totals.bounces = 1 then concat(fullvisitorid, cast(visitstarttime as string)) else null end) as bounces,
  count(distinct case when totals.bounces = 1 then concat(fullvisitorid, cast(visitstarttime as string)) else null end) / count(distinct concat(fullvisitorid, cast(visitstarttime as string))) as bounce_rate,
  sum(totals.timeonsite) / count(distinct concat(fullvisitorid, cast(visitstarttime as string))) as average_session_duration,
  SUM(IF(isExit IS NOT NULL,1,0)) AS exits
FROM
  `isbd-digical.191619457.ga_sessions_*` as GA,
  UNNEST(GA.hits) AS hits
  where totals.visits = 1
  group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28