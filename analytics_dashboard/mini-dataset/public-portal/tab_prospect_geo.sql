#standardSQL
select
    bank_name,
    e_date,
    country,
    Region,
    count (distinct fullVisitorId) as unique_users,
    count (distinct sessionID) as sessions,
    avg(average_session_duration) as avg_session_duration ,
    sum (page_views) as page_views,
    sum (call_me_back) as call_me_back,
    sum (on_boarding_completion) as on_boarding_completion,
    sum (branch_locator) as branch_locator,
    sum (contact_us) as contact_us
from `isbd-analytics-global-dwh.isbd_global_ga_PROD.public_portal_level_2`
where e_date = DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)
group by
    bank_name,
    e_date,
    country,
    Region
