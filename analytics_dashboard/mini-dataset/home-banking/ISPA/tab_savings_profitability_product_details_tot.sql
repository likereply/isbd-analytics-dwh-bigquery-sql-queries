
----insert into `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_savings_profitability_product_details_tot`
with
ispa_db_dis_o as (
 SELECT *,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ) as page_location,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING ) as firebase_screen_class,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
FROM `digical-46e1d.analytics_185824633.events_*` as ga WHERE _TABLE_SUFFIX = DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)

),
ispa_db_dis_oo as (
 select  *,
(case when platform="WEB" then
REGEXP_EXTRACT(
(
SELECT string_agg(url.level_1, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where  platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
), r'([^;]+);?$')
else
 firebase_screen_class
end
) as firebase_screen_class_s,

(case when platform="WEB" then
REGEXP_EXTRACT(
(
SELECT string_agg(url.level_2, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
), r'([^;]+);?$') else
 firebase_screen
end
) as  firebase_screen_s
from ispa_db_dis_o
),

ispa_db_dis as
(SELECT
"ISPA" as bank_name,
PARSE_DATE('%Y%m%d', event_date)as event_date,
event_name,
CONCAT(user_pseudo_id, " - ", event_timestamp) as event_id,
target.target,
fee,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="saving_purchase_id") as STRING ) as saving_purchase_id,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="currency") as STRING ) as currency,
SUM(CAST((SELECT REPLACE(REGEXP_REPLACE(REPLACE(COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)), ",", "."),r"not_found|null", "0"), "[object Object]", "0") FROM UNNEST(event_params) WHERE key="value") as FLOAT64)) * AVG(conversion) as value_eur,
saving_interest_rate,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="saving_tenor") as STRING ) as saving_tenor,
ftp,
user_pseudo_id

FROM ispa_db_dis_oo as ga

LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.isbd_business_metrics` as bm on CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ISPA",ga.firebase_screen_class_s) = join_key_ga
LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.currency_converter` as cc on CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="currency") as STRING )=iso_code
LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.target` as target on target_id=CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ISPA",ga.firebase_screen_class_s)
LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.saving_interest_rate` as saving_ir on id_saving_interest=CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)), "ISPA", CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="saving_name") as STRING ))
INNER JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.stream_id_prod` as sip on ga.stream_id=sip.stream_id
where
ga.firebase_screen_class_s="savings" and event_name="purchase_saving" --togliere transaction
and (REGEXP_CONTAINS(CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ), r"\bdigital.intesasanpaolobank.al\b") or ga.platform in ("ANDROID", "IOS"))
group by 1,2,3,4,5,6,7,8,10,11,12,13),

ispa_db_agg as
(SELECT
bank_name,
event_date,
event_name,
fee,
target,

count(distinct event_id)  as savings_pieces,
SUM(value_eur) as savings_volume,
AVG(ftp) - AVG(saving_interest_rate) as savings_avg_markdown,
AVG(CAST(SUBSTR(saving_tenor, 1, 2) as INT64)*365 + CAST(SUBSTR(saving_tenor, 3, 2) as INT64)*30 + CAST(SUBSTR(saving_tenor, 5, 2) as INT64)) as days_saving_tenor,
AVG(saving_interest_rate) as saving_avg_interest,
COUNT(distinct user_pseudo_id) as savings_active_customer_with_item
from ispa_db_dis
group by 1,2,3,4,5)

SELECT
*,
savings_pieces * fee as savings_income_fees,
savings_volume * savings_avg_markdown  as savings_income_interest,
(savings_pieces * fee)  + (savings_volume * savings_avg_markdown)  as savings_total_income,
savings_volume/savings_pieces as savings_avg_ticket --viene calcolata in pbi
from
ispa_db_agg
order by event_date asc
