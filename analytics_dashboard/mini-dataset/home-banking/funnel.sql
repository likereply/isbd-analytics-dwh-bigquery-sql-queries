with tb_one as (
    select user_pseudo_id,
           event_name,
           event_timestamp, event_date,
           CAST((SELECT CAST(value.string_value as string) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING ) as firebase_screen_class,
           CAST((SELECT CAST(value.string_value as string) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING )       as firebase_screen,
           CAST((SELECT CAST(value.string_value as string) FROM UNNEST(event_params) WHERE key="itemclicked") as STRING )           as itemclicked
    FROM `digical-184607.analytics_163239471.events_20210517` as ga
             INNER JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.stream_id_prod` as sip on ga.stream_id=sip.stream_id
    WHERE (REGEXP_CONTAINS(CAST((SELECT CAST(value.string_value as string) FROM UNNEST(event_params) WHERE key="page_location") as STRING ) , r"\binternetbanking.pbz.hr\b") or ga.platform in ("ANDROID", "IOS"))
      and event_name not in ('user_engagement')
      --and user_pseudo_id in ('001afc4a6bdc090162f3e54d4b5a2212')
    order by user_pseudo_id, event_timestamp asc
), tb_two as (
    select user_pseudo_id,
           event_name, event_date,
           event_timestamp,
           lead(event_name, 1) over(partition by user_pseudo_id order by event_timestamp asc) as next_event_1,
            lead(event_name, 2) over(partition by user_pseudo_id order by event_timestamp asc) as next_event_2,
            lead(event_name, 3) over(partition by user_pseudo_id order by event_timestamp asc) as next_event_3,
            lead(event_name, 4) over(partition by user_pseudo_id order by event_timestamp asc) as next_event_4,
            LEAD(event_name, 5) OVER(PARTITION BY user_pseudo_id ORDER BY event_timestamp ASC) AS next_event_5,
            LEAD(event_name, 6) OVER(PARTITION BY user_pseudo_id ORDER BY event_timestamp ASC) AS next_event_6,
            firebase_screen,
           itemclicked
    from tb_one ), tb_three as (
    select user_pseudo_id,
           event_name, event_date,
           event_timestamp,
           lead(event_name, 1) over(partition by user_pseudo_id order by event_timestamp asc) as next_event_1,
            lead(event_name, 2) over(partition by user_pseudo_id order by event_timestamp asc) as next_event_2,
            lead(event_name, 3) over(partition by user_pseudo_id order by event_timestamp asc) as next_event_3,
            lead(event_name, 4) over(partition by user_pseudo_id order by event_timestamp asc) as next_event_4,
            LEAD(event_name, 5) OVER(PARTITION BY user_pseudo_id ORDER BY event_timestamp ASC) AS next_event_5,
            LEAD(event_name, 6) OVER(PARTITION BY user_pseudo_id ORDER BY event_timestamp ASC) AS next_event_6,
            lead(firebase_screen, 1) over(partition by user_pseudo_id order by event_timestamp asc) as next_firebase_screen_1,
            lead(firebase_screen, 2) over(partition by user_pseudo_id order by event_timestamp asc) as next_firebase_screen_2,
            lead(firebase_screen, 3) over(partition by user_pseudo_id order by event_timestamp asc) as next_firebase_screen_3,
            lead(firebase_screen, 4) over(partition by user_pseudo_id order by event_timestamp asc) as next_firebase_screen_4,
            LEAD(firebase_screen, 5) OVER(PARTITION BY user_pseudo_id ORDER BY event_timestamp ASC) AS next_firebase_screen_5,
            LEAD(firebase_screen, 6) OVER(PARTITION BY user_pseudo_id ORDER BY event_timestamp ASC) AS next_firebase_screen_6,
            lead(itemclicked, 1) over(partition by user_pseudo_id order by event_timestamp asc) as next_itemclicked_1,
            lead(itemclicked, 2) over(partition by user_pseudo_id order by event_timestamp asc) as next_itemclicked_2,
            lead(itemclicked, 3) over(partition by user_pseudo_id order by event_timestamp asc) as next_itemclicked_3,
            lead(itemclicked, 4) over(partition by user_pseudo_id order by event_timestamp asc) as next_itemclicked_4,
            LEAD(itemclicked, 5) OVER(PARTITION BY user_pseudo_id ORDER BY event_timestamp ASC) AS next_itemclicked_5,
            LEAD(itemclicked, 6) OVER(PARTITION BY user_pseudo_id ORDER BY event_timestamp ASC) AS next_itemclicked_6,
            itemclicked
    from tb_two
    where event_name != next_event_1 )
select 'PBZ' as bank_name, event_date, user_pseudo_id, event_name as current_name, event_timestamp,
       ( case when next_itemclicked_1 is not null then next_event_1||':'||ifnull(next_itemclicked_1, '') else next_event_1||':'||ifnull(next_firebase_screen_1, '') end ) as funnel_1,
       ( case when next_itemclicked_2 is not null then next_event_2||':'||ifnull(next_itemclicked_2, '') else next_event_2||':'||ifnull(next_firebase_screen_2, '') end ) as funnel_2,
       ( case when next_itemclicked_3 is not null then next_event_3||':'||ifnull(next_itemclicked_3, '') else next_event_3||':'||ifnull(next_firebase_screen_3, '') end ) as funnel_3,
       ( case when next_itemclicked_4 is not null then next_event_4||':'||ifnull(next_itemclicked_4, '') else next_event_4||':'||ifnull(next_firebase_screen_4, '') end ) as funnel_4,
       ( case when next_itemclicked_3 is not null then next_event_5||':'||ifnull(next_itemclicked_5, '') else next_event_5||':'||ifnull(next_firebase_screen_5, '') end ) as funnel_5,
       ( case when next_itemclicked_4 is not null then next_event_6||':'||ifnull(next_itemclicked_6, '') else next_event_6||':'||ifnull(next_firebase_screen_6, '') end ) as funnel_6,
from tb_three
where event_name in ('click_menu_sticky')
  and itemclicked in ('chat')
order by user_pseudo_id, event_timestamp asc;
