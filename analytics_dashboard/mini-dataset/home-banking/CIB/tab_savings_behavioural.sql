-- add users count and customer service usage
-- add customer service usage

--insert into `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_savings_behavioural`

with
cib_db_dis_o as (
 SELECT *,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ) as page_location,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING ) as firebase_screen_class,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
FROM `isbd-digical.analytics_196589892.events_*` as ga WHERE  _TABLE_SUFFIX = FORMAT_DATE("%Y%m%d", (DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)))

),
cib_db_dis_oo as (
 select  *,
(case when platform="WEB" then
REGEXP_EXTRACT(
(
SELECT string_agg(url.level_1, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where  platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
), r'([^;]+);?$')
else
 firebase_screen_class
end
) as firebase_screen_class_s,

(case when platform="WEB" then
REGEXP_EXTRACT(
(
SELECT string_agg(url.level_2, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
), r'([^;]+);?$') else
 firebase_screen
end
) as  firebase_screen_s
from cib_db_dis_o
),
cib_db_dis as
(SELECT
"CIB" as bank_name,
PARSE_DATE('%Y%m%d', event_date)as event_date,
event_name,
device.category,
device.operating_system,
device.web_info.browser as browser,
step,
function,
fee,
--CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="saving_purchase_id") as STRING ) as saving_purchase_id,
conversion,
saving_interest_rate,
ftp,
user_pseudo_id,
CONCAT(user_pseudo_id, " - ", event_timestamp) as event_id,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="medium") as STRING ) as medium,
SUM(CAST((SELECT REPLACE(REPLACE(REPLACE(COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)), ",", "."),"null", "0"), "[object Object]", "0") FROM UNNEST(event_params) WHERE key="value") as FLOAT64)) * AVG(conversion) as value_eur,

FROM cib_db_dis_oo as ga

LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.isbd_business_metrics` as bm on CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"CIB",ga.firebase_screen_class_s) = join_key_ga
LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.currency_converter` as cc on CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="currency") as STRING )=iso_code
LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.target` as target on target_id=CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"CIB",ga.firebase_screen_class_s)
left join `isbd-analytics-global-dwh.isbd_global_ga_PROD.funnel` on ga.firebase_screen_s=step
LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.saving_interest_rate` as saving_ir on id_saving_interest=CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)), "CIB", CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="saving_name") as STRING ))
INNER JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.stream_id_prod` as sip on ga.stream_id=sip.stream_id
where  _TABLE_SUFFIX = FORMAT_DATE("%Y%m%d", (DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)))
and ga.firebase_screen_class_s = "savings"
and (REGEXP_CONTAINS(CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ), r"\bonline.cib.hu\b") or ga.platform in ("ANDROID", "IOS"))
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15),

cib_db_agg as
(SELECT
bank_name,
event_date,
event_name,
fee,
category,
operating_system,
browser,
step,
function,
case when event_name="contact_us" then medium end as medium,
count(distinct event_id) as interactions,
count(distinct user_pseudo_id) as unique_users,
count(distinct event_id)  as savings_pieces,
SUM(value_eur) as savings_volume,
AVG(ftp) - AVG(saving_interest_rate) as savings_avg_markdown,
from cib_db_dis
group by 1,2,3,4,5,6,7,8,9,10)

SELECT
*,
(savings_pieces * fee)  + (savings_volume * savings_avg_markdown)  as savings_total_income,
from
cib_db_agg
order by event_date asc
