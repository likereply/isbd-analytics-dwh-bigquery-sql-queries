select
"CIB" as bank_name,
PARSE_DATE('%Y%m%d', event_date)as event_date,
count(distinct user_pseudo_id) as unique_users
from `isbd-digical.analytics_196589892.events_*` as ga WHERE  _TABLE_SUFFIX = FORMAT_DATE("%Y%m%d", (DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)))
INNER JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.stream_id_prod` as sip on ga.stream_id=sip.stream_id
where  _TABLE_SUFFIX = FORMAT_DATE("%Y%m%d", (DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)))
and (REGEXP_CONTAINS(CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ), r"\bonline.cib.hu\b") or ga.platform in ("ANDROID", "IOS"))
group by 1,2;
