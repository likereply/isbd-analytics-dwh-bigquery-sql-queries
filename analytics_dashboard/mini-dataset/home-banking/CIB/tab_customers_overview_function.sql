-- add users count and customer service usage
-- add customer service usage

with
cib_db_dis_o as (
SELECT *,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ) as page_location,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING ) as firebase_screen_class,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
FROM `isbd-digical.analytics_196589892.events_*` as ga where  _TABLE_SUFFIX = FORMAT_DATE("%Y%m%d", (DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)))
),
cib_db_dis_oo as (
select  *,
(case when platform="WEB" then
REGEXP_EXTRACT(
(
SELECT string_agg(url.level_1, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where  platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
), r'([^;]+);?$')
else
 firebase_screen_class
end
) as firebase_screen_class_s,
(case when platform="WEB" then
REGEXP_EXTRACT(
(
SELECT string_agg(url.level_2, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
), r'([^;]+);?$') else
 firebase_screen
end
) as  firebase_screen_s
from cib_db_dis_o
)

SELECT
"CIB" as bank_name,
PARSE_DATE('%Y%m%d', event_date)as event_date,
firebase_screen_class_s as section,
count(distinct user_pseudo_id) as unique_users,
FROM cib_db_dis_oo as ga
INNER JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.stream_id_prod` as sip on ga.stream_id=sip.stream_id
where (REGEXP_CONTAINS(ga.page_location, r"\bonline.cib.hu\b") or ga.platform in ("ANDROID", "IOS"))
group by 1,2,3

