
with
    pbz_db_dis_o as (
        SELECT event_params, platform, event_date, stream_id, user_pseudo_id,event_name,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ) as page_location,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="itemclicked") as STRING ) as itemclicked,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING ) as firebase_screen_class,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
        FROM `digical-184607.analytics_163239471.events_*` as ga
    ),
    pbz_db_dis_oo as (
        select  *,
                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_1, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where  platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$')
                      else
                          firebase_screen_class
                    end
                    ) as firebase_screen_class_s,
                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_2, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$')
                      else
                          firebase_screen
                    end
                    ) as  firebase_screen_s
        from pbz_db_dis_o)
SELECT
    event_date,
    firebase_screen_s,
    "PBZ" as bank_name, firebase_screen_class_s, firebase_screen_class_s
    count(distinct user_pseudo_id) as unique_users
FROM pbz_db_dis_oo as ga
         INNER JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.stream_id_prod` as sip on ga.stream_id=sip.stream_id
        -- INNER JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.funnel` as funnel on firebase_screen_s=funnel.step
WHERE (REGEXP_CONTAINS(page_location, r"\binternetbanking.pbz.hr\b") or ga.platform in ("ANDROID", "IOS"))
  and event_name in ("click_menu_sticky") and itemclicked  in ('chat')
group by 1,2,3,4,5