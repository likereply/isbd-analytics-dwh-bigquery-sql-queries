
--insert into `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_cards_demographics`
with

alex_db_dis_o as (
 SELECT *,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ) as page_location,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING ) as firebase_screen_class,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
FROM `digical-boa.analytics_230665710.events_*` as ga --WHERE _TABLE_SUFFIX = DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)

),
alex_db_dis_oo as (
 select  *,
(case when platform="WEB" then
REGEXP_EXTRACT(
(
SELECT string_agg(url.level_1, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where  platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
), r'([^;]+);?$')
else
 firebase_screen_class
end
) as firebase_screen_class_s,

(case when platform="WEB" then
REGEXP_EXTRACT(
(
SELECT string_agg(url.level_2, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
), r'([^;]+);?$') else
 firebase_screen
end
) as  firebase_screen_s
from alex_db_dis_o
),
alex_db_dis as
(SELECT
"ALEX" as bank_name,
PARSE_DATE('%Y%m%d', event_date)as event_date,
event_name,
geo.country,
geo.region as region,
geo.city as city,
target.target,
fee,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="transaction_id") as STRING ) transaction_id,
user_pseudo_id,
SUM(CAST((SELECT REPLACE(REGEXP_REPLACE(REPLACE(COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)), ",", "."),r"not_found|null", "0"), "[object Object]", "0") FROM UNNEST(event_params) WHERE key="value") as FLOAT64)) * AVG(conversion) as value_eur,
conversion,event_timestamp

FROM alex_db_dis_oo as ga

LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.isbd_business_metrics` as bm on CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s) = join_key_ga
LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.currency_converter` as cc on CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="currency") as STRING )=iso_code
LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.target` as target on target_id=CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s)
INNER JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.stream_id_prod` as sip on ga.stream_id=sip.stream_id
where
PARSE_DATE('%Y%m%d', event_date) <= DATE_ADD(CURRENT_DATE(), INTERVAL -2 DAY) and
ga.firebase_screen_class_s="cards" and event_name = "transaction"
and (REGEXP_CONTAINS(CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ), r"\binternetbanking.alexbank.com\b") or ga.platform in ("ANDROID", "IOS"))
group by 1,2,3,4,5,6,7,8,9,10,12,13),

alex_db_agg as
(SELECT
bank_name,
event_date,
event_name,
fee,
target,
country,
region,
city,
SUM(value_eur) cards_volume,
count(distinct ifnull(NULLIF(transaction_id, "[object Object]"), CONCAT(user_pseudo_id, " - ", event_timestamp))) as cards_pieces,
count(distinct user_pseudo_id) as unique_users
from alex_db_dis
group by 1,2,3,4,5,6,7,8)

SELECT
*,
cards_pieces * fee as cards_income,
cards_pieces * fee * 0.059 as cards_income_18_24,
cards_pieces * fee * 0.139 as cards_income_25_34,
cards_pieces * fee * 0.181 as cards_income_35_44,
cards_pieces * fee * 0.163 as cards_income_45_54,
cards_pieces * fee * 0.098 as cards_income_55_64,
cards_pieces * fee * 0.051 as cards_income_over_65,
cards_pieces * fee * 0.309 as cards_income_unknown,
from
alex_db_agg
order by event_date asc
