-- add users count and customer service usage
-- add customer service usage

--insert into `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_loans_behavioural`

with
alex_db_dis_o as (
 SELECT *,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ) as page_location,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING ) as firebase_screen_class,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
FROM `digical-boa.analytics_230665710.events_*` as ga --WHERE _TABLE_SUFFIX = DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)

),
alex_db_dis_oo as (
 select  *,
(case when platform="WEB" then
REGEXP_EXTRACT(
(
SELECT string_agg(url.level_1, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where  platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
), r'([^;]+);?$')
else
 firebase_screen_class
end
) as firebase_screen_class_s,

(case when platform="WEB" then
REGEXP_EXTRACT(
(
SELECT string_agg(url.level_2, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
), r'([^;]+);?$') else
 firebase_screen
end
) as  firebase_screen_s
from alex_db_dis_o
),
alex_db_dis as
(SELECT
"ALEX" as bank_name,
PARSE_DATE('%Y%m%d', event_date)as event_date,
event_name,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
device.category,
device.operating_system,
device.web_info.browser as browser,
step,
function,
fee,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="loan_purchase_id" and event_name in ("purchase_loan", "loan_rejected")) as STRING ) loan_purchase_id,
conversion,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="loan_interest" and event_name in ("purchase_loan", "loan_rejected")) as FLOAT64 ) as loan_interest,
ftp,
user_pseudo_id,
CONCAT(user_pseudo_id, " - ", event_timestamp) as event_id,page_location,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="medium") as STRING ) as medium,
    SUM(CAST((SELECT REPLACE(REGEXP_REPLACE(REPLACE(COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)), ",", "."),r"not_found|null", "0"), "[object Object]", "0") FROM UNNEST(event_params) WHERE key="value" and event_name in ("purchase_loan", "loan_rejected")) as FLOAT64)) * AVG(conversion) as value_eur


FROM alex_db_dis_oo as ga

LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.isbd_business_metrics` as bm on CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s) = join_key_ga
LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.currency_converter` as cc on CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="currency") as STRING )=iso_code
LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.target` as target on target_id=CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s)
left join `isbd-analytics-global-dwh.isbd_global_ga_PROD.funnel` on ga.firebase_screen_s=step
INNER JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.stream_id_prod` as sip on ga.stream_id=sip.stream_id
where
PARSE_DATE('%Y%m%d', event_date) <= DATE_ADD(CURRENT_DATE(), INTERVAL -2 DAY) and  ga.firebase_screen_class_s='loans'
-- and PARSE_DATE('%Y%m%d', event_date) = DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)
and (REGEXP_CONTAINS(CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ), r"\binternetbanking.alexbank.com\b") or ga.platform in ("ANDROID", "IOS"))
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18),

alex_db_agg as
(SELECT
bank_name,
event_date,
event_name,
fee,
category,
operating_system,
browser,
step,
function,
case when event_name="contact_us" then medium end as medium,page_location,
count(distinct event_id) as interactions,
count(distinct user_pseudo_id) as unique_users,
case when event_name="purchase_loan" then count(distinct loan_purchase_id) else 0 end as loans_pieces,
case when event_name="loan_rejected" then count(distinct loan_purchase_id) else 0 end as rejected_loans_pieces,
case when event_name="purchase_loan" then SUM(value_eur) else 0 end as loans_volume,
case when event_name="loan_rejected" then SUM(value_eur) else 0 end as rejected_loans_volume,
case when event_name="purchase_loan" then AVG(loan_interest) - AVG(ftp) else 0 end as loans_avg_markup
from alex_db_dis
group by 1,2,3,4,5,6,7,8,9,10,11)

SELECT
*,
((loans_pieces * fee) - (rejected_loans_pieces * fee)) + ((loans_volume * loans_avg_markup) - (rejected_loans_volume * loans_avg_markup ))  as loans_total_income,
from
alex_db_agg order by event_date asc
