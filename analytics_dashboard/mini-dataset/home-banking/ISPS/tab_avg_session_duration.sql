
with
    level_zero as (
        SELECT
            user_pseudo_id,
            event_date as date,
    timestamp_micros(event_timestamp) as event_timestamp,
    event_name,
    ga.platform as platform,
    CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ) as page_location,
    CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING ) as firebase_screen_class,
    CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
FROM `api-project-808669006573.analytics_204727721.events_*` as ga WHERE _TABLE_SUFFIX = DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)
    INNER JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.stream_id_prod` as sip on ga.stream_id=sip.stream_id
where _TABLE_SUFFIX = DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)
  and (REGEXP_CONTAINS(CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ), r"\bintesasanpaolobank.si\b") or ga.platform in ("ANDROID", "IOS"))
    ),

    level_one as (
select
    *,
    lead(event_name) over (partition by user_pseudo_id order by event_timestamp) as next_event,
    timestamp_diff( lead(event_timestamp) over (partition by user_pseudo_id order by event_timestamp), event_timestamp, second) as time_to_next_event,
    (case when platform="WEB" then
    REGEXP_EXTRACT((SELECT string_agg(url.level_1, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where  platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0), r'([^;]+);?$')
    else
    firebase_screen_class
    end
    ) as firebase_screen_class_s,
    (case when platform="WEB" then
    REGEXP_EXTRACT((SELECT string_agg(url.level_2, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0), r'([^;]+);?$')
    else
    firebase_screen
    end
    ) as  firebase_screen_s
from level_zero
    ),

    sessionize as(
select
    user_pseudo_id,
    event_timestamp,
    date,
    platform,
    firebase_screen_class_s,
    event_name,
    1 + countif(time_to_next_event is null or time_to_next_event > 300) over
    (partition by user_pseudo_id order by event_timestamp asc
    rows between unbounded preceding and 1 preceding) as session_number,
    time_to_next_event
from level_one
    ),

    average_session_duration as (
select
    user_pseudo_id ||'-'|| session_number as session_id,
    'ISPS' as bank_name,
    PARSE_DATE('%Y%m%d', date) as date,
    platform,
    firebase_screen_class_s,
    timestamp_diff(max (event_timestamp), min (event_timestamp), second) as session_duration
from sessionize
group by 1, 2, 3, 4, 5
order by 1
    )

select
    bank_name,
    date,
    platform,
    firebase_screen_class_s,
    avg (session_duration) as avg_session_duration
from average_session_duration
group by 1,2,3,4
