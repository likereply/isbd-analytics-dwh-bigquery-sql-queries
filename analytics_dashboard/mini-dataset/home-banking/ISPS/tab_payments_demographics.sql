
--insert into `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_payments_demographics`
with
isps_db_dis_o as (
 SELECT *,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ) as page_location,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING ) as firebase_screen_class,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
FROM `api-project-808669006573.analytics_204727721.events_*` as ga WHERE _TABLE_SUFFIX = DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)

),
isps_db_dis_oo as (
 select  *,
(case when platform="WEB" then
REGEXP_EXTRACT(
(
SELECT string_agg(url.level_1, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where  platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
), r'([^;]+);?$')
else
 firebase_screen_class
end
) as firebase_screen_class_s,

(case when platform="WEB" then
REGEXP_EXTRACT(
(
SELECT string_agg(url.level_2, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
), r'([^;]+);?$') else
 firebase_screen
end
) as  firebase_screen_s
from isps_db_dis_o
),

isps_db_dis as
(SELECT
"ISPS" as bank_name,
PARSE_DATE('%Y%m%d', event_date)as event_date,
event_name,
geo.country,
geo.region as region,
geo.city as city,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="transaction_type") as STRING ) as transaction_type,
target.target,
fee,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="transaction_id") as STRING ) transaction_id,
user_pseudo_id,
SUM(CAST((SELECT REPLACE(REGEXP_REPLACE(REPLACE(COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)), ",", "."),r"not_found|null", "0"), "[object Object]", "0") FROM UNNEST(event_params) WHERE key="value") as FLOAT64)) * AVG(conversion) as value_eur,
conversion,event_timestamp

FROM isps_db_dis_oo as ga

LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.isbd_business_metrics` as bm on CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ISPS",ga.firebase_screen_class_s) = join_key_ga
LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.currency_converter` as cc on CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="currency") as STRING )=iso_code
LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.target` as target on target_id=CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ISPS",ga.firebase_screen_class_s)
INNER JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.stream_id_prod` as sip on ga.stream_id=sip.stream_id
where
ga.firebase_screen_class_s in ("payments", "gsm_voucher", "cards", "savings") and event_name in ("transaction", "fawry_transaction")
and (REGEXP_CONTAINS(CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ), r"\bintesasanpaolobank.si\b") or ga.platform in ("ANDROID", "IOS"))
group by 1,2,3,4,5,6,7,8,9,10,11,13,14),

isps_db_agg as
(SELECT
bank_name,
event_date,
event_name,
fee,
target,
country,
region,
city,
transaction_type,
SUM(value_eur) as investments_volume,
count(distinct ifnull(NULLIF(transaction_id, "[object Object]"), CONCAT(user_pseudo_id, " - ", event_timestamp))) as payments_pieces,
count(distinct user_pseudo_id) as unique_users
from isps_db_dis
group by 1,2,3,4,5,6,7,8,9)

SELECT
*,
payments_pieces * fee as payments_income,
payments_pieces * fee * 0.059 as payments_income_18_24,
payments_pieces * fee * 0.139 as payments_income_25_34,
payments_pieces * fee * 0.181 as payments_income_35_44,
payments_pieces * fee * 0.163 as payments_income_45_54,
payments_pieces * fee * 0.098 as payments_income_55_64,
payments_pieces * fee * 0.051 as payments_income_over_65,
payments_pieces * fee * 0.309 as payments_income_unknown,
from
isps_db_agg
order by event_date asc
