SELECT
    *
FROM (
         SELECT
             user_pseudo_id,
             event_name,
             event_timestamp,
             LEAD(event_name, 1) OVER(PARTITION BY user_pseudo_id ORDER BY event_timestamp ASC) AS next_event_1,
                 LEAD(event_name, 2) OVER(PARTITION BY user_pseudo_id ORDER BY event_timestamp ASC) AS next_event_2,
                 LEAD(event_name, 3) OVER(PARTITION BY user_pseudo_id ORDER BY event_timestamp ASC) AS next_event_3,
                 LEAD(event_name, 4) OVER(PARTITION BY user_pseudo_id ORDER BY event_timestamp ASC) AS next_event_4,
                 LEAD(event_name, 5) OVER(PARTITION BY user_pseudo_id ORDER BY event_timestamp ASC) AS next_event_5,
                 LEAD(event_name, 6) OVER(PARTITION BY user_pseudo_id ORDER BY event_timestamp ASC) AS next_event_6,
                 LEAD(firebase_screen, 1) OVER(PARTITION BY user_pseudo_id ORDER BY event_timestamp ASC) AS next_firebase_screen_1,
                 LEAD(firebase_screen, 2) OVER(PARTITION BY user_pseudo_id ORDER BY event_timestamp ASC) AS next_firebase_screen_2,
                 LEAD(firebase_screen, 3) OVER(PARTITION BY user_pseudo_id ORDER BY event_timestamp ASC) AS next_firebase_screen_3,
                 LEAD(firebase_screen, 4) OVER(PARTITION BY user_pseudo_id ORDER BY event_timestamp ASC) AS next_firebase_screen_4,
                 LEAD(firebase_screen, 5) OVER(PARTITION BY user_pseudo_id ORDER BY event_timestamp ASC) AS next_firebase_screen_5,
                 LEAD(firebase_screen, 6) OVER(PARTITION BY user_pseudo_id ORDER BY event_timestamp ASC) AS next_firebase_screen_6,
                 LEAD(itemclicked, 1) OVER(PARTITION BY user_pseudo_id ORDER BY event_timestamp ASC) AS next_itemclicked_1,
                 LEAD(itemclicked, 2) OVER(PARTITION BY user_pseudo_id ORDER BY event_timestamp ASC) AS next_itemclicked_2,
                 LEAD(itemclicked, 3) OVER(PARTITION BY user_pseudo_id ORDER BY event_timestamp ASC) AS next_itemclicked_3,
                 LEAD(itemclicked, 4) OVER(PARTITION BY user_pseudo_id ORDER BY event_timestamp ASC) AS next_itemclicked_4,
                 LEAD(itemclicked, 5) OVER(PARTITION BY user_pseudo_id ORDER BY event_timestamp ASC) AS next_itemclicked_5,
                 LEAD(itemclicked, 6) OVER(PARTITION BY user_pseudo_id ORDER BY event_timestamp ASC) AS next_itemclicked_6,
                 itemclicked
         FROM (
                  SELECT
                      user_pseudo_id,
                      event_name,
                      event_timestamp,
                      LEAD(event_name, 1) OVER(PARTITION BY user_pseudo_id ORDER BY event_timestamp ASC) AS next_event_1,
                          LEAD(event_name, 2) OVER(PARTITION BY user_pseudo_id ORDER BY event_timestamp ASC) AS next_event_2,
                          LEAD(event_name, 3) OVER(PARTITION BY user_pseudo_id ORDER BY event_timestamp ASC) AS next_event_3,
                          LEAD(event_name, 4) OVER(PARTITION BY user_pseudo_id ORDER BY event_timestamp ASC) AS next_event_4,
                          LEAD(event_name, 5) OVER(PARTITION BY user_pseudo_id ORDER BY event_timestamp ASC) AS next_event_5,
                          LEAD(event_name, 6) OVER(PARTITION BY user_pseudo_id ORDER BY event_timestamp ASC) AS next_event_6,
                          firebase_screen,
                      itemclicked
                  FROM (
                           SELECT
                               user_pseudo_id,
                               event_name,
                               event_timestamp,
                               CAST((
                                        SELECT
                                            CAST(value.string_value AS string)
                                        FROM
                                            UNNEST(event_params)
                                        WHERE
                                    key="firebase_screen_class") AS STRING ) AS firebase_screen_class,
                               CAST((
                                        SELECT
                                            CAST(value.string_value AS string)
                                        FROM
                                            UNNEST(event_params)
                                        WHERE
                                    key="firebase_screen") AS STRING ) AS firebase_screen,
                               CAST((
                                        SELECT
                                            CAST(value.string_value AS string)
                                        FROM
                                            UNNEST(event_params)
                                        WHERE
                                    key="itemclicked") AS STRING ) AS itemclicked
                           FROM
                               `digical-184607.analytics_163239471.events_20210517`
                           WHERE
                                   event_name NOT IN ('user_engagement')
                             AND user_pseudo_id IN ('001afc4a6bdc090162f3e54d4b5a2212')
                           ORDER BY
                               user_pseudo_id,
                               event_timestamp ASC ))
         WHERE
             event_name != next_event_1 )
WHERE
        event_name IN ('click_menu_sticky')
  AND itemclicked IN ('chat')
ORDER BY
    user_pseudo_id,
    event_timestamp ASC ;