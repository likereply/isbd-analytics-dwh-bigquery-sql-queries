WITH
    pbz_db_dis_o AS (
        SELECT
            event_params,
            platform,
            event_date,
            stream_id,
            user_pseudo_id,
            CAST((
                     SELECT
                         COALESCE(CAST(value.string_value AS string),
                                  CAST(value.int_value AS string),
                                  CAST(value.float_value AS string),
                                  CAST(value.double_value AS string))
                     FROM
                         UNNEST(event_params)
                     WHERE
                 key="page_location") AS STRING ) AS page_location,
            CAST((
                     SELECT
                         COALESCE(CAST(value.string_value AS string),
                                  CAST(value.int_value AS string),
                                  CAST(value.float_value AS string),
                                  CAST(value.double_value AS string))
                     FROM
                         UNNEST(event_params)
                     WHERE
                 key="firebase_screen_class") AS STRING ) AS firebase_screen_class,
            CAST((
                     SELECT
                         COALESCE(CAST(value.string_value AS string),
                                  CAST(value.int_value AS string),
                                  CAST(value.float_value AS string),
                                  CAST(value.double_value AS string))
                     FROM
                         UNNEST(event_params)
                     WHERE
                 key="firebase_screen") AS STRING ) AS firebase_screen,
        FROM
            `digical-184607.analytics_163239471.events_*` AS ga --
        WHERE
                ga.event_name IN ('screen_view',
                                  'page_view') ),
    pbz_db_dis_oo AS (
        SELECT
            *,
            (CASE
                 WHEN platform="WEB" THEN REGEXP_EXTRACT( ( SELECT STRING_AGG(url.level_1, ";" ) FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` AS url WHERE platform=url.regex AND STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0 ), r'([^;]+);?$')
                 ELSE
                     firebase_screen_class
                END
                ) AS firebase_screen_class_s,
            (CASE
                 WHEN platform="WEB" THEN REGEXP_EXTRACT( ( SELECT STRING_AGG(url.level_2, ";" ) FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` AS url WHERE platform=url.regex AND STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0 ), r'([^;]+);?$')
                 ELSE
                     firebase_screen
                END
                ) AS firebase_screen_s
        FROM
            pbz_db_dis_o),
    isps_db_dis_o AS (
        SELECT
            event_params,
            platform,
            event_date,
            stream_id,
            user_pseudo_id,
            CAST((
                     SELECT
                         COALESCE(CAST(value.string_value AS string),
                                  CAST(value.int_value AS string),
                                  CAST(value.float_value AS string),
                                  CAST(value.double_value AS string))
                     FROM
                         UNNEST(event_params)
                     WHERE
                 key="page_location") AS STRING ) AS page_location,
            CAST((
                     SELECT
                         COALESCE(CAST(value.string_value AS string),
                                  CAST(value.int_value AS string),
                                  CAST(value.float_value AS string),
                                  CAST(value.double_value AS string))
                     FROM
                         UNNEST(event_params)
                     WHERE
                 key="firebase_screen_class") AS STRING ) AS firebase_screen_class,
            CAST((
                     SELECT
                         COALESCE(CAST(value.string_value AS string),
                                  CAST(value.int_value AS string),
                                  CAST(value.float_value AS string),
                                  CAST(value.double_value AS string))
                     FROM
                         UNNEST(event_params)
                     WHERE
                 key="firebase_screen") AS STRING ) AS firebase_screen,
        FROM
            `api-project-808669006573.analytics_204727721.events_*` AS ga --
        WHERE
                ga.event_name IN ('screen_view',
                                  'page_view') ),
    isps_db_dis_oo AS (
        SELECT
            *,
            (CASE
                 WHEN platform="WEB" THEN REGEXP_EXTRACT( ( SELECT STRING_AGG(url.level_1, ";" ) FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` AS url WHERE platform=url.regex AND STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0 ), r'([^;]+);?$')
                 ELSE
                     firebase_screen_class
                END
                ) AS firebase_screen_class_s,
            (CASE
                 WHEN platform="WEB" THEN REGEXP_EXTRACT( ( SELECT STRING_AGG(url.level_2, ";" ) FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` AS url WHERE platform=url.regex AND STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0 ), r'([^;]+);?$')
                 ELSE
                     firebase_screen
                END
                ) AS firebase_screen_s
        FROM
            isps_db_dis_o),
    ispa_db_dis_o AS (
        SELECT
            event_params,
            platform,
            event_date,
            stream_id,
            user_pseudo_id,
            CAST((
                     SELECT
                         COALESCE(CAST(value.string_value AS string),
                                  CAST(value.int_value AS string),
                                  CAST(value.float_value AS string),
                                  CAST(value.double_value AS string))
                     FROM
                         UNNEST(event_params)
                     WHERE
                 key="page_location") AS STRING ) AS page_location,
            CAST((
                     SELECT
                         COALESCE(CAST(value.string_value AS string),
                                  CAST(value.int_value AS string),
                                  CAST(value.float_value AS string),
                                  CAST(value.double_value AS string))
                     FROM
                         UNNEST(event_params)
                     WHERE
                 key="firebase_screen_class") AS STRING ) AS firebase_screen_class,
            CAST((
                     SELECT
                         COALESCE(CAST(value.string_value AS string),
                                  CAST(value.int_value AS string),
                                  CAST(value.float_value AS string),
                                  CAST(value.double_value AS string))
                     FROM
                         UNNEST(event_params)
                     WHERE
                 key="firebase_screen") AS STRING ) AS firebase_screen,
        FROM
            `digical-46e1d.analytics_185824633.events_*` AS ga --
        WHERE
                ga.event_name IN ('screen_view',
                                  'page_view') ),
    ispa_db_dis_oo AS (
        SELECT
            *,
            (CASE
                 WHEN platform="WEB" THEN REGEXP_EXTRACT( ( SELECT STRING_AGG(url.level_1, ";" ) FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` AS url WHERE platform=url.regex AND STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0 ), r'([^;]+);?$')
                 ELSE
                     firebase_screen_class
                END
                ) AS firebase_screen_class_s,
            (CASE
                 WHEN platform="WEB" THEN REGEXP_EXTRACT( ( SELECT STRING_AGG(url.level_2, ";" ) FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` AS url WHERE platform=url.regex AND STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0 ), r'([^;]+);?$')
                 ELSE
                     firebase_screen
                END
                ) AS firebase_screen_s
        FROM
            ispa_db_dis_o),
    alex_db_dis_o AS (
        SELECT
            event_params,
            platform,
            event_date,
            stream_id,
            user_pseudo_id,
            CAST((
                     SELECT
                         COALESCE(CAST(value.string_value AS string),
                                  CAST(value.int_value AS string),
                                  CAST(value.float_value AS string),
                                  CAST(value.double_value AS string))
                     FROM
                         UNNEST(event_params)
                     WHERE
                 key="page_location") AS STRING ) AS page_location,
            CAST((
                     SELECT
                         COALESCE(CAST(value.string_value AS string),
                                  CAST(value.int_value AS string),
                                  CAST(value.float_value AS string),
                                  CAST(value.double_value AS string))
                     FROM
                         UNNEST(event_params)
                     WHERE
                 key="firebase_screen_class") AS STRING ) AS firebase_screen_class,
            CAST((
                     SELECT
                         COALESCE(CAST(value.string_value AS string),
                                  CAST(value.int_value AS string),
                                  CAST(value.float_value AS string),
                                  CAST(value.double_value AS string))
                     FROM
                         UNNEST(event_params)
                     WHERE
                 key="firebase_screen") AS STRING ) AS firebase_screen,
        FROM
            `digical-boa.analytics_230665710.events_*` AS ga --
        WHERE
                ga.event_name IN ('screen_view',
                                  'page_view') ),
    alex_db_dis_oo AS (
        SELECT
            *,
            (CASE
                 WHEN platform="WEB" THEN REGEXP_EXTRACT( ( SELECT STRING_AGG(url.level_1, ";" ) FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` AS url WHERE platform=url.regex AND STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0 ), r'([^;]+);?$')
                 ELSE
                     firebase_screen_class
                END
                ) AS firebase_screen_class_s,
            (CASE
                 WHEN platform="WEB" THEN REGEXP_EXTRACT( ( SELECT STRING_AGG(url.level_2, ";" ) FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` AS url WHERE platform=url.regex AND STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0 ), r'([^;]+);?$')
                 ELSE
                     firebase_screen
                END
                ) AS firebase_screen_s
        FROM
            alex_db_dis_o)
SELECT
    DISTINCT(firebase_screen_s) AS step,
            funnel.FUNCTION,
            funnel.macro_section,
            funnel.ORDER,
            "ALEX" AS bank_name,
            CURRENT_DATE() AS update_date,
            COUNT(DISTINCT user_pseudo_id) AS unique_users
FROM
    alex_db_dis_oo AS ga
        INNER JOIN
    `isbd-analytics-global-dwh.isbd_global_ga_PROD.stream_id_prod` AS sip
    ON
            ga.stream_id=sip.stream_id
        INNER JOIN
    `isbd-analytics-global-dwh.isbd_global_ga_PROD.funnel` AS funnel
    ON
            firebase_screen_s=funnel.step
WHERE
    (REGEXP_CONTAINS(page_location, r"\binternetbanking.alexbank.com\b")
        OR ga.platform IN ("ANDROID",
                           "IOS"))
GROUP BY
    1,
    2,
    3,
    4,
    5,
    6
UNION ALL
SELECT
    DISTINCT(firebase_screen_s) AS step,
            funnel.FUNCTION,
            funnel.macro_section,
            funnel.ORDER,
            "ISPA" AS bank_name,
            CURRENT_DATE() AS update_date,
            COUNT(DISTINCT user_pseudo_id) AS unique_users
FROM
    ispa_db_dis_oo AS ga
        INNER JOIN
    `isbd-analytics-global-dwh.isbd_global_ga_PROD.stream_id_prod` AS sip
    ON
            ga.stream_id=sip.stream_id
        INNER JOIN
    `isbd-analytics-global-dwh.isbd_global_ga_PROD.funnel` AS funnel
    ON
            firebase_screen_s=funnel.step
WHERE
    (REGEXP_CONTAINS(page_location, r"\bdigital.intesasanpaolobank.al\b")
        OR ga.platform IN ("ANDROID",
                           "IOS"))
GROUP BY
    1,
    2,
    3,
    4,
    5,
    6
UNION ALL
SELECT
    DISTINCT(firebase_screen_s) AS step,
            funnel.FUNCTION,
            funnel.macro_section,
            funnel.ORDER,
            "ISPS" AS bank_name,
            CURRENT_DATE() AS update_date,
            COUNT(DISTINCT user_pseudo_id) AS unique_users
FROM
    isps_db_dis_oo AS ga
        INNER JOIN
    `isbd-analytics-global-dwh.isbd_global_ga_PROD.stream_id_prod` AS sip
    ON
            ga.stream_id=sip.stream_id
        INNER JOIN
    `isbd-analytics-global-dwh.isbd_global_ga_PROD.funnel` AS funnel
    ON
            firebase_screen_s=funnel.step
WHERE
    (REGEXP_CONTAINS(page_location, r"\bintesasanpaolobank.si\b")
        OR ga.platform IN ("ANDROID",
                           "IOS"))
GROUP BY
    1,
    2,
    3,
    4,
    5,
    6
UNION ALL
SELECT
    DISTINCT(firebase_screen_s) AS step,
            funnel.FUNCTION,
            funnel.macro_section,
            funnel.ORDER,
            "PBZ" AS bank_name,
            CURRENT_DATE() AS update_date,
            COUNT(DISTINCT user_pseudo_id) AS unique_users
FROM
    pbz_db_dis_oo AS ga
        INNER JOIN
    `isbd-analytics-global-dwh.isbd_global_ga_PROD.stream_id_prod` AS sip
    ON
            ga.stream_id=sip.stream_id
        INNER JOIN
    `isbd-analytics-global-dwh.isbd_global_ga_PROD.funnel` AS funnel
    ON
            firebase_screen_s=funnel.step
WHERE
    (REGEXP_CONTAINS(page_location, r"\binternetbanking.pbz.hr\b")
        OR ga.platform IN ("ANDROID",
                           "IOS"))
GROUP BY
    1,
    2,
    3,
    4,
    5,
    6
