with step0 as (
    SELECT event_params, platform, event_date, stream_id, user_pseudo_id,event_name, event_timestamp,
           CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ) as page_location,
           CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="itemclicked") as STRING ) as itemclicked
    FROM `digical-184607.analytics_163239471.events_20210517` as ga where event_name not in ('user_engagement')
),
     step1 as (
         select event_params, platform, event_date, stream_id, user_pseudo_id,event_name, event_timestamp,
                CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ) as page_location,
                CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING ) as firebase_screen_class,
                CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
                CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="itemclicked") as STRING ) as itemclicked
         from step0
     ),
     step2 as (
         select
             user_pseudo_id,stream_id,page_location,platform,itemclicked,
             event_name as current_event, event_timestamp,
             LEAD(event_name,1) OVER (PARTITION BY user_pseudo_id ORDER BY event_timestamp) AS next_event_1,
                 LEAD(event_name,2) OVER (PARTITION BY user_pseudo_id ORDER BY event_timestamp) AS next_event_2,
                 LEAD(event_name,3) OVER (PARTITION BY user_pseudo_id ORDER BY event_timestamp) AS next_event_3
         from step1
     )
select * from step2 ga INNER JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.stream_id_prod` as sip on ga.stream_id=sip.stream_id
WHERE (REGEXP_CONTAINS(page_location, r"\binternetbanking.pbz.hr\b") or ga.platform in ("ANDROID", "IOS"))
  and current_event in ("click_menu_sticky") and itemclicked  in ('chat')order by user_pseudo_id, event_timestamp asc
