
--insert into `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_loans_demographics`
with
pbz_db_dis_o as (
 SELECT *,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ) as page_location,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING ) as firebase_screen_class,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
FROM `digical-184607.analytics_163239471.events_*` as ga WHERE _TABLE_SUFFIX = DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)

),
pbz_db_dis_oo as (
 select  *,
(case when platform="WEB" then
REGEXP_EXTRACT(
(
SELECT string_agg(url.level_1, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where  platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
), r'([^;]+);?$')
else
 firebase_screen_class
end
) as firebase_screen_class_s,

(case when platform="WEB" then
REGEXP_EXTRACT(
(
SELECT string_agg(url.level_2, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
), r'([^;]+);?$') else
 firebase_screen
end
) as  firebase_screen_s
from pbz_db_dis_o
),
pbz_db_dis as
(SELECT
"PBZ" as bank_name,
PARSE_DATE('%Y%m%d', event_date)as event_date,
event_name,
geo.region as region,
geo.city as city,
target.target,
fee,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="loan_purchase_id") as STRING ) loan_purchase_id,
SUM(CAST((SELECT REPLACE(REPLACE(REPLACE(COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)), ",", "."),"null", "0"), "[object Object]", "0") FROM UNNEST(event_params) WHERE key="value") as FLOAT64)) * AVG(conversion) as value_eur,
conversion,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="loan_interest") as FLOAT64 ) as loan_interest,
CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="loan_tenor") as INT64 ) as loan_tenor,
ftp,
user_pseudo_id

FROM pbz_db_dis_oo as ga

LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.isbd_business_metrics` as bm on CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"PBZ",ga.firebase_screen_class_s) = join_key_ga
LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.currency_converter` as cc on CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="currency") as STRING )=iso_code
LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.target` as target on target_id=CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"PBZ",ga.firebase_screen_class_s)
INNER JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.stream_id_prod` as sip on ga.stream_id=sip.stream_id
where
ga.firebase_screen_class_s="loans" and event_name in ("purchase_loan", "loan_rejected")
and (REGEXP_CONTAINS(CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ), r"\binternetbanking.pbz.hr\b") or ga.platform in ("ANDROID", "IOS"))
group by 1,2,3,4,5,6,7,8,10,11,12,13,14),

pbz_db_agg as
(SELECT
bank_name,
event_date,
event_name,
fee,
target,
region,
city,
case when event_name="purchase_loan" then count(distinct loan_purchase_id) else 0 end as loans_pieces,
case when event_name="loan_rejected" then count(distinct loan_purchase_id) else 0 end as rejected_loans_pieces,
case when event_name="purchase_loan" then SUM(value_eur) else 0 end as loans_volume,
case when event_name="loan_rejected" then SUM(value_eur) else 0 end as rejected_loans_volume,
case when event_name="purchase_loan" then AVG(loan_interest) - AVG(ftp) else 0 end as loans_avg_markup,
case when event_name="purchase_loan" then AVG(loan_tenor) else 0 end as loans_avg_tenor,
case when event_name="purchase_loan" then AVG(loan_interest) else 0 end as loans_avg_interest,
case when event_name="purchase_loan" then COUNT(distinct user_pseudo_id) else 0 end as loans_active_customer_with_item
from pbz_db_dis
group by 1,2,3,4,5,6,7)

SELECT
*,
(loans_pieces * fee) - (rejected_loans_pieces * fee) as loans_income_fees,
(loans_volume * loans_avg_markup) - (rejected_loans_volume * loans_avg_markup )  as loans_income_interest,
((loans_pieces * fee) - (rejected_loans_pieces * fee)) + ((loans_volume * loans_avg_markup) - (rejected_loans_volume * loans_avg_markup ))  as loans_total_income,
loans_volume/loans_pieces as avg_ticket, --viene calcolata in pbi
(((loans_pieces * fee) - (rejected_loans_pieces * fee)) + ((loans_volume * loans_avg_markup) - (rejected_loans_volume * loans_avg_markup ))) * 0.059 as loans_income_18_24,
(((loans_pieces * fee) - (rejected_loans_pieces * fee)) + ((loans_volume * loans_avg_markup) - (rejected_loans_volume * loans_avg_markup ))) * 0.139 as loans_income_25_34,
(((loans_pieces * fee) - (rejected_loans_pieces * fee)) + ((loans_volume * loans_avg_markup) - (rejected_loans_volume * loans_avg_markup ))) * 0.181 as loans_income_35_44,
(((loans_pieces * fee) - (rejected_loans_pieces * fee)) + ((loans_volume * loans_avg_markup) - (rejected_loans_volume * loans_avg_markup ))) * 0.163 as loans_income_45_54,
(((loans_pieces * fee) - (rejected_loans_pieces * fee)) + ((loans_volume * loans_avg_markup) - (rejected_loans_volume * loans_avg_markup ))) * 0.098 as loans_income_55_64,
(((loans_pieces * fee) - (rejected_loans_pieces * fee)) + ((loans_volume * loans_avg_markup) - (rejected_loans_volume * loans_avg_markup ))) * 0.051 as loans_income_over_65,
(((loans_pieces * fee) - (rejected_loans_pieces * fee)) + ((loans_volume * loans_avg_markup) - (rejected_loans_volume * loans_avg_markup ))) * 0.309 as loans_income_unknown,
from
pbz_db_agg
order by event_date asc
