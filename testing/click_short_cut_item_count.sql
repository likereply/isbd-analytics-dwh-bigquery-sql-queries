SELECT
COUNTIF(value like "%-%") as both,
COUNTIF(value = "withsave") as withsave,
COUNTIF(value = "saving_withsave") as saving_withsave
FROM
(SELECT
user_pseudo_id,
STRING_AGG(distinct db.key, "-") as key,
STRING_AGG(distinct db.value.string_value, "-") as value
FROM `digical-184607.analytics_163239471.events_*`,
UNNEST(event_params) as db
where event_name in ("click_pre_login_shortcut", "click_main_menu") and PARSE_DATE('%Y%m%d', event_date) >= "2020-12-21" and PARSE_DATE('%Y%m%d', event_date) <= "2021-01-19" and db.key in ("click_shortcut","itemclicked") and db.value.string_value in ("withsave","saving_withsave")
and stream_id in ("1882516406", "1411574163", "2201252152")
group by 1)
