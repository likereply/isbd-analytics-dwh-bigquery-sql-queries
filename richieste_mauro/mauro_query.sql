with
db as
(SELECT
--CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING)  as firebase_screen_class,
count(distinct user_pseudo_id) as users,
--count(user_pseudo_id) as users,
(SELECT COUNT(distinct CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="transaction_id") as STRING ))
FROM `digical-184607.analytics_163239471.events*`
where CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING) in ('payments','gsm_voucher')
and event_name="transaction" and device.operating_system = 'iOS'
and (device.operating_system_version like 'iOS 10%' or device.operating_system_version like 'iOS 9%')
and event_date >= '20210101') as payments,
(SELECT sum(CAST((SELECT REPLACE(REPLACE(REPLACE(COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)), ",", "."),"null", "0"), "[object Object]", "0") FROM UNNEST(event_params) WHERE key="value") as FLOAT64)*cc.conversion)
FROM `digical-184607.analytics_163239471.events*`
where CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING) in ('payments','gsm_voucher') and event_name="transaction" ) as payments_volume_EUR,
(SELECT COUNT(
distinct CAST((
SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string))
FROM UNNEST(event_params)
WHERE key="loan_purchase_id") as STRING
)
)
FROM `digical-184607.analytics_163239471.events*`
where event_name = "purchase_loan" and device.operating_system = 'iOS'
and (device.operating_system_version like 'iOS 10%' or device.operating_system_version like 'iOS 9%')
and event_date >= '20210101') as loans,
(SELECT COUNT (distinct(CONCAT(user_pseudo_id, " - ", event_timestamp)))
FROM `digical-184607.analytics_163239471.events*`
where CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING) = "savings"
and (event_name="transaction" or event_name="purchase_saving" and device.operating_system = 'iOS'
and (device.operating_system_version like 'iOS 10%' or device.operating_system_version like 'iOS 9%')
and event_date >= '20210101')) as savings,
(SELECT count(distinct CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="transaction_id") as STRING ))
FROM `digical-184607.analytics_163239471.events*`
where CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING )="investment" and event_name in ("fund_purchase", "fund_sell") and device.operating_system = 'iOS'
and (device.operating_system_version like 'iOS 10%' or device.operating_system_version like 'iOS 9%')
and event_date >= '20210101') as investments,
FROM `digical-184607.analytics_163239471.events*`
LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.currency_converter` as cc on CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="currency") as STRING )=iso_code
where device.operating_system = 'iOS'
and (device.operating_system_version like 'iOS 10%' or device.operating_system_version like 'iOS 9%')
and event_date >= '20210101'
--and CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING) in ('payments','gsm_voucher','investment', 'savings', 'loans')
group by cc.conversion)

SELECT
SUM(users) as users,
AVG(payments) as payments,
sum(payments_volume_EUR) as payments_volume_EUR,
AVG(loans) as loans,
AVG(savings) as savings,
AVG(investments) as investments,
SUM(users * 0.059) as users_18_24,
SUM(users * 0.139) as users_25_34,
SUM(users * 0.181) as users_35_44,
SUM(users * 0.163) as users_45_54,
SUM(users * 0.098) as users_55_64,
SUM(users * 0.051) as users_over_65,
SUM(users * 0.309) as users_unknown,

from db
