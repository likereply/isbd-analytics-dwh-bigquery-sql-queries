with lvl0 as (
    select *,
           (CASE
                WHEN ((traffic_source.source="direct" AND traffic_source.medium="(not set)") OR traffic_source.medium="(none)") THEN "Direct"
                WHEN traffic_source.medium="organic" THEN "Organic Search"
                WHEN REGEXP_CONTAINS(traffic_source.source,"^(.*social.*|.*social-network.*|.*social-media.*|sm|.*social network.*|.*social media.*)$") THEN "Social"
                WHEN traffic_source.medium="email" THEN "Email"
                WHEN traffic_source.medium="affiliate" THEN "Affiliates"
                WHEN traffic_source.medium="referral" THEN "Referral"
                WHEN (REGEXP_CONTAINS(traffic_source.medium,"^(.*cpc.*|.*ppc.*|.*paidsearch.*)$ "))  THEN "Paid Search"
                WHEN REGEXP_CONTAINS(traffic_source.medium,"^(.*cpv.*|.*cpa.*|.*cpp.*|.*content-text.*)$") THEN "Other Advertising"
                WHEN (REGEXP_CONTAINS(traffic_source.medium,"^(.*display.*|.*cpm.*|.*banner.*)$")) THEN "Display"
                WHEN (REGEXP_CONTAINS(traffic_source.medium,"^(.*native.*)$")) THEN "Native"
                WHEN NOT(REGEXP_CONTAINS(traffic_source.source,"^(.*social.*|.*social-network.*|.*social-media.*|sm|.*social network.*|.*social media.*)$")) AND (REGEXP_CONTAINS(traffic_source.medium,"^(.*video.*)$")) THEN "Video"
                ELSE "(Other)"
               END) as channel,
           CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ) as page_location,
           CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING ) as firebase_screen_class,
           CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
    FROM `digical-184607.analytics_163239471.events_20210101` -- where event_date between '20210301' and '20210301'
),

     lvl1 as (
         SELECT *,
                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_1, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where  platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$')
                      else
                          firebase_screen_class
                    end
                    ) as screen_class,
                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_2, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$') else
                          firebase_screen
                    end
                    ) as screen_view
         from lvl0
     ),

     lvl2 as (
         select
             channel,
             screen_view,
             case when event_name="purchase_loan" then 1 else 0 end as converted,
             case when event_name="purchase_loan" then event_date else "0" end as converted_date,
             user_pseudo_id,
             (select count(*) from `digical-184607.analytics_163239471.events_*` as innerga where event_name="login" and innerga.user_pseudo_id = ga.user_pseudo_id ) as n_login,
             (select DATE_DIFF(DATE (max(PARSE_DATE('%Y%m%d', event_date))), DATE (min(PARSE_DATE('%Y%m%d', event_date))), DAY) from `digical-184607.analytics_163239471.events_*` as innerga where innerga.user_pseudo_id = ga.user_pseudo_id ) as vita
         FROM lvl1 as ga
                  INNER JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.stream_id_prod` as sip on ga.stream_id=sip.stream_id
         where screen_class="loans"
           and (REGEXP_CONTAINS(page_location, r"\binternetbanking.pbz.hr\b") or ga.platform in ("ANDROID", "IOS"))
     )

select
    converted, user_pseudo_id, converted_date, n_login, vita, channel
from lvl2 group by 1,2,3,4,5, 6;

