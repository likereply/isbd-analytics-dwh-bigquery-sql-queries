DELETE FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_cards_behavioural` where bank_name in ('ALEX');
DELETE FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_cards_demographics` where bank_name in ('ALEX');
DELETE FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_cards_profitability_product_details_tot` where bank_name in ('ALEX');

DELETE FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_investments_behavioural` where bank_name in ('ALEX');
DELETE FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_investments_demographics` where bank_name in ('ALEX');
DELETE FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_investments_profitability_product_details_tot` where bank_name in ('ALEX');

DELETE FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_payments_behavioural` where bank_name in ('ALEX');
DELETE FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_payments_demographics` where bank_name in ('ALEX');
DELETE FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_payments_profitability_product_details_tot` where bank_name in ('ALEX');

DELETE FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_loans_behavioural` where bank_name in ('ALEX');
DELETE FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_loans_demographics` where bank_name in ('ALEX');
DELETE FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_loans_profitability_product_details_tot` where bank_name in ('ALEX');

DELETE FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_savings_behavioural` where bank_name in ('ALEX');
DELETE FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_savings_demographics` where bank_name in ('ALEX');
DELETE FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_savings_profitability_product_details_tot` where bank_name in ('ALEX');

SELECT DISTINCT(bank_name) FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_cards_behavioural` ;
SELECT DISTINCT(bank_name) FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_cards_demographics` ;
SELECT DISTINCT(bank_name) FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_cards_profitability_product_details_tot` ;

SELECT DISTINCT(bank_name) FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_investments_behavioural` ;
SELECT DISTINCT(bank_name) FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_investments_demographics` ;
SELECT DISTINCT(bank_name) FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_investments_profitability_product_details_tot` ;

SELECT DISTINCT(bank_name) FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_payments_behavioural` ;
SELECT DISTINCT(bank_name) FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_payments_demographics` ;
SELECT DISTINCT(bank_name) FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_payments_profitability_product_details_tot` ;

SELECT DISTINCT(bank_name) FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_loans_behavioural` ;
SELECT DISTINCT(bank_name) FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_loans_demographics` ;
SELECT DISTINCT(bank_name) FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_loans_profitability_product_details_tot` ;

SELECT DISTINCT(bank_name) FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_savings_behavioural` ;
SELECT DISTINCT(bank_name) FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_savings_demographics` ;
SELECT DISTINCT(bank_name) FROM `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_savings_profitability_product_details_tot` ;
