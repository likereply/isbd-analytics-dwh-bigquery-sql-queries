-- add users count and customer service usage
-- add customer service usage

insert into `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_cards_behavioural`

with
    alex_db_dis_o as (
        SELECT *,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ) as page_location,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING ) as firebase_screen_class,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
        FROM `digical-boa.analytics_230665710.events_*` as ga --WHERE _TABLE_SUFFIX = DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)

    ),
    alex_db_dis_oo as (
        select  *,
                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_1, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where  platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$')
                      else
                          firebase_screen_class
                    end
                    ) as firebase_screen_class_s,

                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_2, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$') else
                          firebase_screen
                    end
                    ) as  firebase_screen_s
        from alex_db_dis_o
    ),
    alex_db_dis as
        (SELECT
             "ALEX" as bank_name,
             PARSE_DATE('%Y%m%d', event_date)as event_date,
             event_name,
--CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
             device.category,
             device.operating_system,
             device.web_info.browser as browser,
             step,
             function,
             fee,
             conversion,
             user_pseudo_id,
             CONCAT(user_pseudo_id, " - ", event_timestamp) as event_id,event_timestamp,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="medium") as STRING ) as medium,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="transaction_id") as STRING ) transaction_id,
             SUM(CAST((SELECT REPLACE(REGEXP_REPLACE(REPLACE(COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)), ",", "."),r"not_found|null", "0"), "[object Object]", "0") FROM UNNEST(event_params) WHERE key="value") as FLOAT64)) * AVG(conversion) as value_eur,

         FROM alex_db_dis_oo as ga

                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.isbd_business_metrics` as bm on CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s) = join_key_ga
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.currency_converter` as cc on CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="currency") as STRING )=iso_code
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.target` as target on target_id=CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s)
                  left join `isbd-analytics-global-dwh.isbd_global_ga_PROD.funnel` on ga.firebase_screen_s=step
                  INNER JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.stream_id_prod` as sip on ga.stream_id=sip.stream_id
         where
                 PARSE_DATE('%Y%m%d', event_date) <= DATE_ADD(CURRENT_DATE(), INTERVAL -2 DAY) and
                 ga.firebase_screen_class_s = "cards"
           and (REGEXP_CONTAINS(CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ), r"\binternetbanking.alexbank.com\b") or ga.platform in ("ANDROID", "IOS"))
         group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15),

    alex_db_agg as
        (SELECT
             bank_name,
             event_date,
             event_name,
             fee,
             category,
             operating_system,
             browser,
             step,
             function,
             case when event_name="contact_us" then medium end as medium,
             count(distinct event_id) as cards_interactions, -- se si vuole mettere come naming cards_interactions
             SUM(value_eur) as cards_volume,
             count(distinct ifnull(NULLIF(transaction_id, "[object Object]"), CONCAT(user_pseudo_id, " - ", event_timestamp))) as cards_pieces,
             count(distinct user_pseudo_id) as unique_users
         from alex_db_dis
         group by 1,2,3,4,5,6,7,8,9,10)

SELECT
    *,
    cards_pieces * fee as cards_income
from
    alex_db_agg
order by event_date asc;


insert into `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_cards_demographics`
with

    alex_db_dis_o as (
        SELECT *,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ) as page_location,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING ) as firebase_screen_class,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
        FROM `digical-boa.analytics_230665710.events_*` as ga --WHERE _TABLE_SUFFIX = DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)

    ),
    alex_db_dis_oo as (
        select  *,
                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_1, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where  platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$')
                      else
                          firebase_screen_class
                    end
                    ) as firebase_screen_class_s,

                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_2, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$') else
                          firebase_screen
                    end
                    ) as  firebase_screen_s
        from alex_db_dis_o
    ),
    alex_db_dis as
        (SELECT
             "ALEX" as bank_name,
             PARSE_DATE('%Y%m%d', event_date)as event_date,
             event_name,
             geo.country,
             geo.region as region,
             geo.city as city,
             target.target,
             fee,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="transaction_id") as STRING ) transaction_id,
             user_pseudo_id,
             SUM(CAST((SELECT REPLACE(REGEXP_REPLACE(REPLACE(COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)), ",", "."),r"not_found|null", "0"), "[object Object]", "0") FROM UNNEST(event_params) WHERE key="value") as FLOAT64)) * AVG(conversion) as value_eur,
             conversion,event_timestamp

         FROM alex_db_dis_oo as ga

                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.isbd_business_metrics` as bm on CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s) = join_key_ga
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.currency_converter` as cc on CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="currency") as STRING )=iso_code
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.target` as target on target_id=CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s)
                  INNER JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.stream_id_prod` as sip on ga.stream_id=sip.stream_id
         where
                 PARSE_DATE('%Y%m%d', event_date) <= DATE_ADD(CURRENT_DATE(), INTERVAL -2 DAY) and
                 ga.firebase_screen_class_s="cards" and event_name = "transaction"
           and (REGEXP_CONTAINS(CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ), r"\binternetbanking.alexbank.com\b") or ga.platform in ("ANDROID", "IOS"))
         group by 1,2,3,4,5,6,7,8,9,10,12,13),

    alex_db_agg as
        (SELECT
             bank_name,
             event_date,
             event_name,
             fee,
             target,
             country,
             region,
             city,
             SUM(value_eur) cards_volume,
             count(distinct ifnull(NULLIF(transaction_id, "[object Object]"), CONCAT(user_pseudo_id, " - ", event_timestamp))) as cards_pieces,
             count(distinct user_pseudo_id) as unique_users
         from alex_db_dis
         group by 1,2,3,4,5,6,7,8)

SELECT
    *,
    cards_pieces * fee as cards_income,
    cards_pieces * fee * 0.059 as cards_income_18_24,
    cards_pieces * fee * 0.139 as cards_income_25_34,
    cards_pieces * fee * 0.181 as cards_income_35_44,
    cards_pieces * fee * 0.163 as cards_income_45_54,
    cards_pieces * fee * 0.098 as cards_income_55_64,
    cards_pieces * fee * 0.051 as cards_income_over_65,
    cards_pieces * fee * 0.309 as cards_income_unknown,
from
    alex_db_agg
order by event_date asc;

insert into `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_cards_profitability_product_details_tot`
with
    alex_db_dis_o as (
        SELECT *,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ) as page_location,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING ) as firebase_screen_class,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
        FROM `digical-boa.analytics_230665710.events_*` as ga --WHERE _TABLE_SUFFIX = DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)

    ),
    alex_db_dis_oo as (
        select  *,
                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_1, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where  platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$')
                      else
                          firebase_screen_class
                    end
                    ) as firebase_screen_class_s,

                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_2, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$') else
                          firebase_screen
                    end
                    ) as  firebase_screen_s
        from alex_db_dis_o
    ),
    alex_db_dis as
        (SELECT
             "ALEX" as bank_name,
             PARSE_DATE('%Y%m%d', event_date)as event_date,
             event_name,
             target.target,
             fee,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="transaction_id") as STRING ) transaction_id,
             user_pseudo_id,
             SUM(CAST((SELECT REPLACE(REGEXP_REPLACE(REPLACE(COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)), ",", "."),r"not_found|null", "0"), "[object Object]", "0") FROM UNNEST(event_params) WHERE key="value") as FLOAT64)) * AVG(conversion) as value_eur,
             conversion,event_timestamp

         FROM alex_db_dis_oo as ga

                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.isbd_business_metrics` as bm on CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s) = join_key_ga
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.currency_converter` as cc on CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="currency") as STRING )=iso_code
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.target` as target on target_id=CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s)
                  INNER JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.stream_id_prod` as sip on ga.stream_id=sip.stream_id
         where
                 PARSE_DATE('%Y%m%d', event_date) <= DATE_ADD(CURRENT_DATE(), INTERVAL -2 DAY) and
                 ga.firebase_screen_class_s="cards" and event_name = "transaction"
           and (REGEXP_CONTAINS(CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ), r"\binternetbanking.alexbank.com\b") or ga.platform in ("ANDROID", "IOS"))
         group by 1,2,3,4,5,6,7,9,10),

    alex_db_agg as
        (SELECT
             bank_name,
             event_date,
             event_name,
             fee,
             target,
             SUM(value_eur) as cards_volume,
             count(distinct ifnull(NULLIF(transaction_id, "[object Object]"), CONCAT(user_pseudo_id, " - ", event_timestamp))) as cards_pieces,
             count(distinct user_pseudo_id) as unique_users
         from alex_db_dis
         group by 1,2,3,4,5)

SELECT
    *,
    cards_pieces * fee as cards_income
from
    alex_db_agg
order by event_date asc;
-- add users count and customer service usage
-- add customer service usage

insert into `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_investments_behavioural`

with
    alex_db_dis_o as (
        SELECT *,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ) as page_location,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING ) as firebase_screen_class,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
        FROM `digical-boa.analytics_230665710.events_*` as ga --WHERE _TABLE_SUFFIX = DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)

    ),
    alex_db_dis_oo as (
        select  *,
                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_1, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where  platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$')
                      else
                          firebase_screen_class
                    end
                    ) as firebase_screen_class_s,

                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_2, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$') else
                          firebase_screen
                    end
                    ) as  firebase_screen_s
        from alex_db_dis_o
    ),
    alex_db_dis as
        (SELECT
             "ALEX" as bank_name,
             PARSE_DATE('%Y%m%d', event_date)as event_date,
             event_name,
--CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
             device.category,
             device.operating_system,
             device.web_info.browser as browser,
             step,
             function,
             fee,
             conversion,
             user_pseudo_id,
             CONCAT(user_pseudo_id, " - ", event_timestamp) as event_id,event_timestamp,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="medium") as STRING ) as medium,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="fund_name") as STRING ) as fund_name,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="transaction_id") as STRING ) transaction_id,
             SUM(CAST((SELECT REPLACE(REGEXP_REPLACE(REPLACE(COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)), ",", "."),r"not_found|null", "0"), "[object Object]", "0") FROM UNNEST(event_params) WHERE key="value") as FLOAT64)) * AVG(conversion) as value_eur,


         FROM alex_db_dis_oo as ga

                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.isbd_business_metrics` as bm on CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s) = join_key_ga
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.currency_converter` as cc on CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="currency") as STRING )=iso_code
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.target` as target on target_id=CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s)
                  left join `isbd-analytics-global-dwh.isbd_global_ga_PROD.funnel` on ga.firebase_screen_s=step
                  INNER JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.stream_id_prod` as sip on ga.stream_id=sip.stream_id
         where
                 PARSE_DATE('%Y%m%d', event_date) <= DATE_ADD(CURRENT_DATE(), INTERVAL -2 DAY)
           and ga.firebase_screen_class_s = "investment"
           and (REGEXP_CONTAINS(CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ), r"\binternetbanking.alexbank.com\b") or ga.platform in ("ANDROID", "IOS"))
         group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16),

    alex_db_agg as
        (SELECT
             bank_name,
             event_date,
             event_name,
             fee,
             category,
             operating_system,
             browser,
             step,
             function,
             fund_name,
             case when event_name="contact_us" then medium end as medium,
             count(distinct event_id) as interactions,
             SUM(value_eur) as investments_volume,
             count(distinct ifnull(NULLIF(transaction_id, "[object Object]"), CONCAT(user_pseudo_id, " - ", event_timestamp))) as investments_pieces,
             count(distinct user_pseudo_id) as unique_traders
         from alex_db_dis
         group by 1,2,3,4,5,6,7,8,9,10,11)

SELECT
    *,
    investments_pieces * fee as investments_income
from
    alex_db_agg
order by event_date asc;

insert into `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_investments_demographics`
with

    alex_db_dis_o as (
        SELECT *,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ) as page_location,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING ) as firebase_screen_class,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
        FROM `digical-boa.analytics_230665710.events_*` as ga --WHERE _TABLE_SUFFIX = DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)

    ),
    alex_db_dis_oo as (
        select  *,
                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_1, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where  platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$')
                      else
                          firebase_screen_class
                    end
                    ) as firebase_screen_class_s,

                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_2, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$') else
                          firebase_screen
                    end
                    ) as  firebase_screen_s
        from alex_db_dis_o
    ),
    alex_db_dis as
        (SELECT
             "ALEX" as bank_name,
             PARSE_DATE('%Y%m%d', event_date)as event_date,
             event_name,
             geo.country,
             geo.region as region,
             geo.city as city,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="fund_name") as STRING ) as fund_name,
             target.target,
             fee,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="transaction_id") as STRING ) transaction_id,
             user_pseudo_id,
             SUM(CAST((SELECT REPLACE(REGEXP_REPLACE(REPLACE(COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)), ",", "."),r"not_found|null", "0"), "[object Object]", "0") FROM UNNEST(event_params) WHERE key="value") as FLOAT64)) * AVG(conversion) as value_eur,
             conversion,event_timestamp

         FROM alex_db_dis_oo as ga

                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.isbd_business_metrics` as bm on CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s) = join_key_ga
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.currency_converter` as cc on CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="currency") as STRING )=iso_code
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.target` as target on target_id=CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s)
                  INNER JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.stream_id_prod` as sip on ga.stream_id=sip.stream_id
         where
                 PARSE_DATE('%Y%m%d', event_date) <= DATE_ADD(CURRENT_DATE(), INTERVAL -2 DAY) and
                 ga.firebase_screen_class_s="investment" and event_name in ("fund_purchase")
           and (REGEXP_CONTAINS(CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ), r"\binternetbanking.alexbank.com\b") or ga.platform in ("ANDROID", "IOS"))
         group by 1,2,3,4,5,6,7,8,9,10,11,13,14),

    alex_db_agg as
        (SELECT
             bank_name,
             event_date,
             event_name,
             fee,
             target,
             country,
             region,
             city,
             fund_name,
             SUM(value_eur) as investments_volume,
             count(distinct ifnull(NULLIF(transaction_id, "[object Object]"), CONCAT(user_pseudo_id, " - ", event_timestamp))) as investments_pieces,
             count(distinct user_pseudo_id) as unique_traders
         from alex_db_dis
         group by 1,2,3,4,5,6,7,8,9)

SELECT
    *,
    investments_pieces * fee as investments_income,
    investments_pieces * fee * 0.059 as investments_income_18_24,
    investments_pieces * fee * 0.139 as investments_income_25_34,
    investments_pieces * fee * 0.181 as investments_income_35_44,
    investments_pieces * fee * 0.163 as investments_income_45_54,
    investments_pieces * fee * 0.098 as investments_income_55_64,
    investments_pieces * fee * 0.051 as investments_income_over_65,
    investments_pieces * fee * 0.309 as investments_income_unknown
from
    alex_db_agg
order by event_date asc;

insert into `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_investments_profitability_product_details_tot`
with
    alex_db_dis_o as (
        SELECT *,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ) as page_location,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING ) as firebase_screen_class,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
        FROM `digical-boa.analytics_230665710.events_*` as ga --WHERE _TABLE_SUFFIX = DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)

    ),
    alex_db_dis_oo as (
        select  *,
                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_1, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where  platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$')
                      else
                          firebase_screen_class
                    end
                    ) as firebase_screen_class_s,

                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_2, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$') else
                          firebase_screen
                    end
                    ) as  firebase_screen_s
        from alex_db_dis_o
    ),
    alex_db_dis as
        (SELECT
             "ALEX" as bank_name,
             PARSE_DATE('%Y%m%d', event_date)as event_date,
             event_name,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="fund_name") as STRING ) as fund_name,
             target.target,
             fee,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="transaction_id") as STRING ) transaction_id,
             user_pseudo_id,
             SUM(CAST((SELECT REPLACE(REGEXP_REPLACE(REPLACE(COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)), ",", "."),r"not_found|null", "0"), "[object Object]", "0") FROM UNNEST(event_params) WHERE key="value") as FLOAT64)) * AVG(conversion) as value_eur,
             conversion,event_timestamp

         FROM alex_db_dis_oo as ga

                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.isbd_business_metrics` as bm on CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s) = join_key_ga
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.currency_converter` as cc on CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="currency") as STRING )=iso_code
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.target` as target on target_id=CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s)
                  INNER JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.stream_id_prod` as sip on ga.stream_id=sip.stream_id
         where
                 PARSE_DATE('%Y%m%d', event_date) <= DATE_ADD(CURRENT_DATE(), INTERVAL -2 DAY) and
                 ga.firebase_screen_class_s="investment" and event_name in ("fund_purchase")--togliere i sell
           and (REGEXP_CONTAINS(CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ), r"\binternetbanking.alexbank.com\b") or ga.platform in ("ANDROID", "IOS"))
         group by 1,2,3,4,5,6,7,8,10,11),

    alex_db_agg as
        (SELECT
             bank_name,
             event_date,
             event_name,
             fund_name,
             fee,
             target,
             SUM(value_eur) as investments_volume,
             count(distinct ifnull(NULLIF(transaction_id, "[object Object]"), CONCAT(user_pseudo_id, " - ", event_timestamp))) as investments_pieces,
             count(distinct user_pseudo_id) as unique_traders
         from alex_db_dis
         group by 1,2,3,4,5,6)

SELECT
    *,
    investments_pieces * fee as investments_income
from
    alex_db_agg
order by event_date asc;
-- add users count and customer service usage
-- add customer service usage

insert into `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_loans_behavioural`

with
    alex_db_dis_o as (
        SELECT *,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ) as page_location,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING ) as firebase_screen_class,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
        FROM `digical-boa.analytics_230665710.events_*` as ga --WHERE _TABLE_SUFFIX = DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)

    ),
    alex_db_dis_oo as (
        select  *,
                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_1, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where  platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$')
                      else
                          firebase_screen_class
                    end
                    ) as firebase_screen_class_s,

                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_2, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$') else
                          firebase_screen
                    end
                    ) as  firebase_screen_s
        from alex_db_dis_o
    ),
    alex_db_dis as
        (SELECT
             "ALEX" as bank_name,
             PARSE_DATE('%Y%m%d', event_date)as event_date,
             event_name,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
             device.category,
             device.operating_system,
             device.web_info.browser as browser,
             step,
             function,
             fee,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="loan_purchase_id" and event_name in ("purchase_loan", "loan_rejected")) as STRING ) loan_purchase_id,
             conversion,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="loan_interest" and event_name in ("purchase_loan", "loan_rejected")) as FLOAT64 ) as loan_interest,
             ftp,
             user_pseudo_id,
             CONCAT(user_pseudo_id, " - ", event_timestamp) as event_id,page_location,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="medium") as STRING ) as medium,
             SUM(CAST((SELECT REPLACE(REGEXP_REPLACE(REPLACE(COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)), ",", "."),r"not_found|null", "0"), "[object Object]", "0") FROM UNNEST(event_params) WHERE key="value" and event_name in ("purchase_loan", "loan_rejected")) as FLOAT64)) * AVG(conversion) as value_eur


         FROM alex_db_dis_oo as ga

                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.isbd_business_metrics` as bm on CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s) = join_key_ga
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.currency_converter` as cc on CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="currency") as STRING )=iso_code
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.target` as target on target_id=CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s)
                  left join `isbd-analytics-global-dwh.isbd_global_ga_PROD.funnel` on ga.firebase_screen_s=step
                  INNER JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.stream_id_prod` as sip on ga.stream_id=sip.stream_id
         where
                 PARSE_DATE('%Y%m%d', event_date) <= DATE_ADD(CURRENT_DATE(), INTERVAL -2 DAY) and  ga.firebase_screen_class_s='loans'
-- and PARSE_DATE('%Y%m%d', event_date) = DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)
           and (REGEXP_CONTAINS(CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ), r"\binternetbanking.alexbank.com\b") or ga.platform in ("ANDROID", "IOS"))
         group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18),

    alex_db_agg as
        (SELECT
             bank_name,
             event_date,
             event_name,
             fee,
             category,
             operating_system,
             browser,
             step,
             function,
             case when event_name="contact_us" then medium end as medium,page_location,
             count(distinct event_id) as interactions,
             count(distinct user_pseudo_id) as unique_users,
             case when event_name="purchase_loan" then count(distinct loan_purchase_id) else 0 end as loans_pieces,
             case when event_name="loan_rejected" then count(distinct loan_purchase_id) else 0 end as rejected_loans_pieces,
             case when event_name="purchase_loan" then SUM(value_eur) else 0 end as loans_volume,
             case when event_name="loan_rejected" then SUM(value_eur) else 0 end as rejected_loans_volume,
             case when event_name="purchase_loan" then AVG(loan_interest) - AVG(ftp) else 0 end as loans_avg_markup
         from alex_db_dis
         group by 1,2,3,4,5,6,7,8,9,10,11)

SELECT
    *,
    ((loans_pieces * fee) - (rejected_loans_pieces * fee)) + ((loans_volume * loans_avg_markup) - (rejected_loans_volume * loans_avg_markup ))  as loans_total_income,
from
    alex_db_agg order by event_date asc;

insert into `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_loans_demographics`
with
    alex_db_dis_o as (
        SELECT *,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ) as page_location,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING ) as firebase_screen_class,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
        FROM `digical-boa.analytics_230665710.events_*` as ga --WHERE _TABLE_SUFFIX = DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)

    ),
    alex_db_dis_oo as (
        select  *,
                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_1, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where  platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$')
                      else
                          firebase_screen_class
                    end
                    ) as firebase_screen_class_s,

                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_2, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$') else
                          firebase_screen
                    end
                    ) as  firebase_screen_s
        from alex_db_dis_o
    ),
    alex_db_dis as
        (SELECT
             "ALEX" as bank_name,
             PARSE_DATE('%Y%m%d', event_date)as event_date,
             event_name,
             geo.region as region,
             geo.city as city,
             target.target,
             fee,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="loan_purchase_id") as STRING ) loan_purchase_id,
             SUM(CAST((SELECT REPLACE(REGEXP_REPLACE(REPLACE(COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)), ",", "."),r"not_found|null", "0"), "[object Object]", "0") FROM UNNEST(event_params) WHERE key="value") as FLOAT64)) * AVG(conversion) as value_eur,
             conversion,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="loan_interest") as FLOAT64 ) as loan_interest,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="loan_tenor") as INT64 ) as loan_tenor,
             ftp,
             user_pseudo_id

         FROM alex_db_dis_oo as ga

                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.isbd_business_metrics` as bm on CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s) = join_key_ga
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.currency_converter` as cc on CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="currency") as STRING )=iso_code
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.target` as target on target_id=CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s)
                  INNER JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.stream_id_prod` as sip on ga.stream_id=sip.stream_id
         where
                 PARSE_DATE('%Y%m%d', event_date) <= DATE_ADD(CURRENT_DATE(), INTERVAL -2 DAY) and
                 ga.firebase_screen_class_s="loans" and event_name in ("purchase_loan", "loan_rejected")
           and (REGEXP_CONTAINS(CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ), r"\binternetbanking.alexbank.com\b") or ga.platform in ("ANDROID", "IOS"))
         group by 1,2,3,4,5,6,7,8,10,11,12,13,14),

    alex_db_agg as
        (SELECT
             bank_name,
             event_date,
             event_name,
             fee,
             target,
             region,
             city,
             case when event_name="purchase_loan" then count(distinct loan_purchase_id) else 0 end as loans_pieces,
             case when event_name="loan_rejected" then count(distinct loan_purchase_id) else 0 end as rejected_loans_pieces,
             case when event_name="purchase_loan" then SUM(value_eur) else 0 end as loans_volume,
             case when event_name="loan_rejected" then SUM(value_eur) else 0 end as rejected_loans_volume,
             case when event_name="purchase_loan" then AVG(loan_interest) - AVG(ftp) else 0 end as loans_avg_markup,
             case when event_name="purchase_loan" then AVG(loan_tenor) else 0 end as loans_avg_tenor,
             case when event_name="purchase_loan" then AVG(loan_interest) else 0 end as loans_avg_interest,
             case when event_name="purchase_loan" then COUNT(distinct user_pseudo_id) else 0 end as loans_active_customer_with_item
         from alex_db_dis
         group by 1,2,3,4,5,6,7)

SELECT
    *,
    (loans_pieces * fee) - (rejected_loans_pieces * fee) as loans_income_fees,
    (loans_volume * loans_avg_markup) - (rejected_loans_volume * loans_avg_markup )  as loans_income_interest,
    ((loans_pieces * fee) - (rejected_loans_pieces * fee)) + ((loans_volume * loans_avg_markup) - (rejected_loans_volume * loans_avg_markup ))  as loans_total_income,
    loans_volume/loans_pieces as avg_ticket, --viene calcolata in pbi
    (((loans_pieces * fee) - (rejected_loans_pieces * fee)) + ((loans_volume * loans_avg_markup) - (rejected_loans_volume * loans_avg_markup ))) * 0.059 as loans_income_18_24,
    (((loans_pieces * fee) - (rejected_loans_pieces * fee)) + ((loans_volume * loans_avg_markup) - (rejected_loans_volume * loans_avg_markup ))) * 0.139 as loans_income_25_34,
    (((loans_pieces * fee) - (rejected_loans_pieces * fee)) + ((loans_volume * loans_avg_markup) - (rejected_loans_volume * loans_avg_markup ))) * 0.181 as loans_income_35_44,
    (((loans_pieces * fee) - (rejected_loans_pieces * fee)) + ((loans_volume * loans_avg_markup) - (rejected_loans_volume * loans_avg_markup ))) * 0.163 as loans_income_45_54,
    (((loans_pieces * fee) - (rejected_loans_pieces * fee)) + ((loans_volume * loans_avg_markup) - (rejected_loans_volume * loans_avg_markup ))) * 0.098 as loans_income_55_64,
    (((loans_pieces * fee) - (rejected_loans_pieces * fee)) + ((loans_volume * loans_avg_markup) - (rejected_loans_volume * loans_avg_markup ))) * 0.051 as loans_income_over_65,
    (((loans_pieces * fee) - (rejected_loans_pieces * fee)) + ((loans_volume * loans_avg_markup) - (rejected_loans_volume * loans_avg_markup ))) * 0.309 as loans_income_unknown,
from
    alex_db_agg
order by event_date asc;


insert into `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_loans_profitability_product_details_tot`
with
    alex_db_dis_o as (
        SELECT *,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ) as page_location,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING ) as firebase_screen_class,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
        FROM `digical-boa.analytics_230665710.events_*` as ga --WHERE _TABLE_SUFFIX = DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)

    ),
    alex_db_dis_oo as (
        select  *,
                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_1, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where  platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$')
                      else
                          firebase_screen_class
                    end
                    ) as firebase_screen_class_s,

                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_2, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$') else
                          firebase_screen
                    end
                    ) as  firebase_screen_s
        from alex_db_dis_o
    ),
    alex_db_dis as
        (SELECT
             "ALEX" as bank_name,
             PARSE_DATE('%Y%m%d', event_date)as event_date,
             event_name,
             target.target,
             fee,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="loan_purchase_id") as STRING ) loan_purchase_id,
             (CASE
                  WHEN (ifnull(CAST((SELECT REPLACE(REPLACE(REPLACE(COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)), ",", "."),"null", "0"), "[object Object]", "0") FROM UNNEST(event_params) WHERE key="value") as FLOAT64),0) * ifnull(conversion,0))>0 and (ifnull(CAST((SELECT REPLACE(REPLACE(REPLACE(COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)), ",", "."),"null", "0"), "[object Object]", "0") FROM UNNEST(event_params) WHERE key="value") as FLOAT64),0) * ifnull(conversion,0))<=2000  THEN "1-2000"
                  WHEN (ifnull(CAST((SELECT REPLACE(REPLACE(REPLACE(COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)), ",", "."),"null", "0"), "[object Object]", "0") FROM UNNEST(event_params) WHERE key="value") as FLOAT64),0) * ifnull(conversion,0))>2000 and (ifnull(CAST((SELECT REPLACE(REPLACE(REPLACE(COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)), ",", "."),"null", "0"), "[object Object]", "0") FROM UNNEST(event_params) WHERE key="value") as FLOAT64),0) * ifnull(conversion,0))<=6000  THEN "2000-6000"
                  WHEN (ifnull(CAST((SELECT REPLACE(REPLACE(REPLACE(COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)), ",", "."),"null", "0"), "[object Object]", "0") FROM UNNEST(event_params) WHERE key="value") as FLOAT64),0) * ifnull(conversion,0))>6000 and (ifnull(CAST((SELECT REPLACE(REPLACE(REPLACE(COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)), ",", "."),"null", "0"), "[object Object]", "0") FROM UNNEST(event_params) WHERE key="value") as FLOAT64),0) * ifnull(conversion,0))<=12000  THEN "6000-12000"
                  WHEN (ifnull(CAST((SELECT REPLACE(REPLACE(REPLACE(COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)), ",", "."),"null", "0"), "[object Object]", "0") FROM UNNEST(event_params) WHERE key="value") as FLOAT64),0) * ifnull(conversion,0))>12000  THEN ">12000"
                  else null
                 END) as loans_cluster_amount,
             SUM(CAST((SELECT REPLACE(REGEXP_REPLACE(REPLACE(COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)), ",", "."),r"not_found|null", "0"), "[object Object]", "0") FROM UNNEST(event_params) WHERE key="value") as FLOAT64)) * AVG(conversion) as value_eur,
             conversion,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="loan_interest") as FLOAT64 ) as loan_interest,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="loan_tenor") as INT64 ) as loan_tenor,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="loan_insurance") as STRING ) as loan_insurance,
             ftp,
             user_pseudo_id

         FROM alex_db_dis_oo as ga

                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.isbd_business_metrics` as bm on CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s) = join_key_ga
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.currency_converter` as cc on CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="currency") as STRING )=iso_code
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.target` as target on target_id=CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s)
                  INNER JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.stream_id_prod` as sip on ga.stream_id=sip.stream_id
         where
                 PARSE_DATE('%Y%m%d', event_date) <= DATE_ADD(CURRENT_DATE(), INTERVAL -2 DAY) and
                 ga.firebase_screen_class_s="loans" and event_name in ("purchase_loan", "loan_rejected")
           and (REGEXP_CONTAINS(CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ), r"\binternetbanking.alexbank.com\b") or ga.platform in ("ANDROID", "IOS"))
         group by 1,2,3,4,5,6,7,9,10,11,12,13,14),

    alex_db_agg as
        (SELECT
             bank_name,
             event_date,
             event_name,
             fee,
             target,
             loans_cluster_amount,
             loan_insurance,
             case when event_name="purchase_loan" then count(distinct loan_purchase_id) else 0 end as loans_pieces,
--case when event_name="loan_rejected" then count(distinct loan_purchase_id) else 0 end as rejected_loans_pieces,
             case when event_name="purchase_loan" and loan_insurance="yes" then count(distinct loan_purchase_id) else 0 end as cpi_pieces,
--case when event_name="loan_rejected" and loan_insurance="yes" then count(distinct loan_purchase_id) else 0 end as rejected_cpi_pieces,
             case when event_name="loan_rejected" then count(distinct loan_purchase_id) else 0 end as rejected_loans_pieces,
             case when event_name="purchase_loan" then SUM(value_eur) else 0 end as loans_volume,
             case when event_name="purchase_loan" and loan_insurance="yes" then SUM(value_eur) else 0 end as cpi_volume,
--case when event_name="loan_rejected" and loan_insurance="yes" then SUM(value_eur) else 0 end as rejected_cpi_volume,
             case when event_name="loan_rejected" then SUM(value_eur) else 0 end as rejected_loans_volume,
             case when event_name="purchase_loan" then AVG(loan_interest) - AVG(ftp) else 0 end as loans_avg_markup,
             case when event_name="purchase_loan" then AVG(loan_tenor) else 0 end as loans_avg_tenor,
             case when event_name="purchase_loan" then AVG(loan_interest) else 0 end as loans_avg_interest,
             case when event_name="purchase_loan" then COUNT(distinct user_pseudo_id) else 0 end as loans_active_customer_with_item
         from alex_db_dis
         group by 1,2,3,4,5,6,7)

SELECT
    *,
    loans_volume - rejected_loans_volume as loans_net_volume,
    (loans_pieces * fee) - (rejected_loans_pieces * fee) as loans_income_fees,
    (loans_volume * loans_avg_markup) - (rejected_loans_volume * loans_avg_markup )  as loans_income_interest,
    ((loans_pieces * fee) - (rejected_loans_pieces * fee)) + ((loans_volume * loans_avg_markup) - (rejected_loans_volume * loans_avg_markup ))  as loans_total_income,
    loans_volume/loans_pieces as loans_avg_ticket, --viene calcolata in pbi
    cpi_pieces/loans_pieces as cpi_over_loans_pieces, --viene calcolata in pbi
    (loans_volume - rejected_loans_volume) * 0.4 as cpi_target
from
    alex_db_agg
order by event_date asc;
-- add users count and customer service usage
-- add customer service usage

insert into `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_payments_behavioural`

with
    alex_db_dis_o as (
        SELECT *,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ) as page_location,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING ) as firebase_screen_class,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
        FROM `digical-boa.analytics_230665710.events_*` as ga --WHERE _TABLE_SUFFIX = DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)

    ),
    alex_db_dis_oo as (
        select  *,
                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_1, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where  platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$')
                      else
                          firebase_screen_class
                    end
                    ) as firebase_screen_class_s,

                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_2, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$') else
                          firebase_screen
                    end
                    ) as  firebase_screen_s
        from alex_db_dis_o
    ),
    alex_db_dis as
        (SELECT
             "ALEX" as bank_name,
             PARSE_DATE('%Y%m%d', event_date)as event_date,
             event_name,
--CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
             device.category,
             device.operating_system,
             device.web_info.browser as browser,
             step,
             function,
             fee,
             conversion,
             user_pseudo_id,
             CONCAT(user_pseudo_id, " - ", event_timestamp) as event_id,event_timestamp,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="medium") as STRING ) as medium,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="transaction_type") as STRING ) as transaction_type,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="transaction_id") as STRING ) transaction_id,
             SUM(CAST((SELECT REPLACE(REGEXP_REPLACE(REPLACE(COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)), ",", "."),r"not_found|null", "0"), "[object Object]", "0") FROM UNNEST(event_params) WHERE key="value") as FLOAT64)) * AVG(conversion) as value_eur,


         FROM alex_db_dis_oo as ga

                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.isbd_business_metrics` as bm on CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s) = join_key_ga
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.currency_converter` as cc on CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="currency") as STRING )=iso_code
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.target` as target on target_id=CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s)
                  left join `isbd-analytics-global-dwh.isbd_global_ga_PROD.funnel` on ga.firebase_screen_s=step
                  INNER JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.stream_id_prod` as sip on ga.stream_id=sip.stream_id
         where
                 PARSE_DATE('%Y%m%d', event_date) <= DATE_ADD(CURRENT_DATE(), INTERVAL -2 DAY) and
                 ga.firebase_screen_class_s in ("payments", "gsm_voucher", "cards", "savings")
           and (REGEXP_CONTAINS(CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ), r"\binternetbanking.alexbank.com\b") or ga.platform in ("ANDROID", "IOS"))
         group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16),

    alex_db_agg as
        (SELECT
             bank_name,
             event_date,
             event_name,
             fee,
             category,
             operating_system,
             browser,
             step,
             function,
             transaction_type,
             case when event_name="contact_us" then medium end as medium,
             count(distinct event_id) as interactions,
             SUM(value_eur) as payments_volume,
             count(distinct ifnull(NULLIF(transaction_id, "[object Object]"), CONCAT(user_pseudo_id, " - ", event_timestamp))) as payments_pieces,
             count(distinct user_pseudo_id) as unique_users
         from alex_db_dis
         group by 1,2,3,4,5,6,7,8,9,10,11)

SELECT
    *,
    payments_pieces * fee as payments_income
from
    alex_db_agg
order by event_date asc;

insert into `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_payments_demographics`
with
    alex_db_dis_o as (
        SELECT *,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ) as page_location,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING ) as firebase_screen_class,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
        FROM `digical-boa.analytics_230665710.events_*` as ga --WHERE _TABLE_SUFFIX = DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)

    ),
    alex_db_dis_oo as (
        select  *,
                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_1, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where  platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$')
                      else
                          firebase_screen_class
                    end
                    ) as firebase_screen_class_s,

                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_2, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$') else
                          firebase_screen
                    end
                    ) as  firebase_screen_s
        from alex_db_dis_o
    ),

    alex_db_dis as
        (SELECT
             "ALEX" as bank_name,
             PARSE_DATE('%Y%m%d', event_date)as event_date,
             event_name,
             geo.country,
             geo.region as region,
             geo.city as city,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="transaction_type") as STRING ) as transaction_type,
             target.target,
             fee,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="transaction_id") as STRING ) transaction_id,
             user_pseudo_id,
             SUM(CAST((SELECT REPLACE(REGEXP_REPLACE(REPLACE(COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)), ",", "."),r"not_found|null", "0"), "[object Object]", "0") FROM UNNEST(event_params) WHERE key="value") as FLOAT64)) * AVG(conversion) as value_eur,
             conversion,event_timestamp

         FROM alex_db_dis_oo as ga

                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.isbd_business_metrics` as bm on CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s) = join_key_ga
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.currency_converter` as cc on CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="currency") as STRING )=iso_code
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.target` as target on target_id=CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s)
                  INNER JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.stream_id_prod` as sip on ga.stream_id=sip.stream_id
         where
                 PARSE_DATE('%Y%m%d', event_date) <= DATE_ADD(CURRENT_DATE(), INTERVAL -2 DAY)
           and ga.firebase_screen_class_s in ("payments", "gsm_voucher", "cards", "savings") and event_name in ("transaction", "fawry_transaction")
           and (REGEXP_CONTAINS(CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ), r"\binternetbanking.alexbank.com\b") or ga.platform in ("ANDROID", "IOS"))
         group by 1,2,3,4,5,6,7,8,9,10,11,13,14),

    alex_db_agg as
        (SELECT
             bank_name,
             event_date,
             event_name,
             fee,
             target,
             country,
             region,
             city,
             transaction_type,
             SUM(value_eur) as investments_volume,
             count(distinct ifnull(NULLIF(transaction_id, "[object Object]"), CONCAT(user_pseudo_id, " - ", event_timestamp))) as payments_pieces,
             count(distinct user_pseudo_id) as unique_users
         from alex_db_dis
         group by 1,2,3,4,5,6,7,8,9)

SELECT
    *,
    payments_pieces * fee as payments_income,
    payments_pieces * fee * 0.059 as payments_income_18_24,
    payments_pieces * fee * 0.139 as payments_income_25_34,
    payments_pieces * fee * 0.181 as payments_income_35_44,
    payments_pieces * fee * 0.163 as payments_income_45_54,
    payments_pieces * fee * 0.098 as payments_income_55_64,
    payments_pieces * fee * 0.051 as payments_income_over_65,
    payments_pieces * fee * 0.309 as payments_income_unknown,
from
    alex_db_agg
order by event_date asc;

insert into `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_payments_profitability_product_details_tot`
with
    alex_db_dis_o as (
        SELECT *,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ) as page_location,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING ) as firebase_screen_class,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
        FROM `digical-boa.analytics_230665710.events_*` as ga --WHERE _TABLE_SUFFIX = DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)

    ),
    alex_db_dis_oo as (
        select  *,
                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_1, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where  platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$')
                      else
                          firebase_screen_class
                    end
                    ) as firebase_screen_class_s,

                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_2, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$') else
                          firebase_screen
                    end
                    ) as  firebase_screen_s
        from alex_db_dis_o
    ),
    alex_db_dis as
        (SELECT
             "ALEX" as bank_name,
             PARSE_DATE('%Y%m%d', event_date)as event_date,
             event_name,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="transaction_type") as STRING ) as transaction_type,
             target.target,
             fee,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="transaction_id") as STRING ) transaction_id,
             user_pseudo_id,
             SUM(CAST((SELECT REPLACE(REGEXP_REPLACE(REPLACE(COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)), ",", "."),r"not_found|null", "0"), "[object Object]", "0") FROM UNNEST(event_params) WHERE key="value") as FLOAT64)) * AVG(conversion) as value_eur,
             conversion,event_timestamp

         FROM alex_db_dis_oo as ga

                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.isbd_business_metrics` as bm on CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s) = join_key_ga
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.currency_converter` as cc on CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="currency") as STRING )=iso_code
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.target` as target on target_id=CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s)
                  INNER JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.stream_id_prod` as sip on ga.stream_id=sip.stream_id
         where
                 PARSE_DATE('%Y%m%d', event_date) <= DATE_ADD(CURRENT_DATE(), INTERVAL -2 DAY) and
                 ga.firebase_screen_class_s in ("payments", "gsm_voucher", "cards", "savings") and event_name in ("transaction", "fawry_transaction") -- inserire cards, savings perchè fanno parte della stessa cosa nel tab
           and (REGEXP_CONTAINS(CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ), r"\binternetbanking.alexbank.com\b") or ga.platform in ("ANDROID", "IOS"))
         group by 1,2,3,4,5,6,7,8,10,11),

    alex_db_agg as
        (SELECT
             bank_name,
             event_date,
             event_name,
             transaction_type,
             fee,
             target,
             SUM(value_eur) as payments_volume,
             count(distinct ifnull(NULLIF(transaction_id, "[object Object]"), CONCAT(user_pseudo_id, " - ", event_timestamp))) as payments_pieces,
             count(distinct user_pseudo_id) as unique_users
         from alex_db_dis
         group by 1,2,3,4,5,6)
SELECT
    *,
    payments_pieces * fee as payments_income
from
    alex_db_agg
order by event_date asc;
-- add users count and customer service usage
-- add customer service usage

insert into `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_savings_behavioural`

with
    alex_db_dis_o as (
        SELECT *,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ) as page_location,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING ) as firebase_screen_class,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
        FROM `digical-boa.analytics_230665710.events_*` as ga --WHERE _TABLE_SUFFIX = DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)

    ),
    alex_db_dis_oo as (
        select  *,
                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_1, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where  platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$')
                      else
                          firebase_screen_class
                    end
                    ) as firebase_screen_class_s,

                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_2, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$') else
                          firebase_screen
                    end
                    ) as  firebase_screen_s
        from alex_db_dis_o
    ),
    alex_db_dis as
        (SELECT
             "ALEX" as bank_name,
             PARSE_DATE('%Y%m%d', event_date)as event_date,
             event_name,
             device.category,
             device.operating_system,
             device.web_info.browser as browser,
             step,
             function,
             fee,
--CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="saving_purchase_id") as STRING ) as saving_purchase_id,
             conversion,
             saving_interest_rate,
             ftp,
             user_pseudo_id,
             CONCAT(user_pseudo_id, " - ", event_timestamp) as event_id,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="medium") as STRING ) as medium,
             SUM(CAST((SELECT REPLACE(REGEXP_REPLACE(REPLACE(COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)), ",", "."),r"not_found|null", "0"), "[object Object]", "0") FROM UNNEST(event_params) WHERE key="value") as FLOAT64)) * AVG(conversion) as value_eur,

         FROM alex_db_dis_oo as ga

                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.isbd_business_metrics` as bm on CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s) = join_key_ga
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.currency_converter` as cc on CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="currency") as STRING )=iso_code
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.target` as target on target_id=CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s)
                  left join `isbd-analytics-global-dwh.isbd_global_ga_PROD.funnel` on ga.firebase_screen_s=step
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.saving_interest_rate` as saving_ir on id_saving_interest=CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)), "ALEX", CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="saving_name") as STRING ))
                  INNER JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.stream_id_prod` as sip on ga.stream_id=sip.stream_id
         where
                 PARSE_DATE('%Y%m%d', event_date) <= DATE_ADD(CURRENT_DATE(), INTERVAL -2 DAY) and
                 ga.firebase_screen_class_s = "savings"
           and (REGEXP_CONTAINS(CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ), r"\binternetbanking.alexbank.com\b") or ga.platform in ("ANDROID", "IOS"))
         group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15),

    alex_db_agg as
        (SELECT
             bank_name,
             event_date,
             event_name,
             fee,
             category,
             operating_system,
             browser,
             step,
             function,
             case when event_name="contact_us" then medium end as medium,
             count(distinct event_id) as interactions,
             count(distinct user_pseudo_id) as unique_users,
             count(distinct event_id)  as savings_pieces,
             SUM(value_eur) as savings_volume,
             AVG(ftp) - AVG(saving_interest_rate) as savings_avg_markdown,
         from alex_db_dis
         group by 1,2,3,4,5,6,7,8,9,10)

SELECT
    *,
    (savings_pieces * fee)  + (savings_volume * savings_avg_markdown)  as savings_total_income,
from
    alex_db_agg
order by event_date asc;

insert into `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_savings_demographics`
with
    alex_db_dis_o as (
        SELECT *,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ) as page_location,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING ) as firebase_screen_class,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
        FROM `digical-boa.analytics_230665710.events_*` as ga --WHERE _TABLE_SUFFIX = DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)

    ),
    alex_db_dis_oo as (
        select  *,
                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_1, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where  platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$')
                      else
                          firebase_screen_class
                    end
                    ) as firebase_screen_class_s,

                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_2, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$') else
                          firebase_screen
                    end
                    ) as  firebase_screen_s
        from alex_db_dis_o
    ),

    alex_db_dis as
        (SELECT
             "ALEX" as bank_name,
             PARSE_DATE('%Y%m%d', event_date)as event_date,
             event_name,
             geo.country,
             geo.region as region,
             geo.city as city,
             target.target,
             fee,
             CONCAT(user_pseudo_id, " - ", event_timestamp) as event_id,
--CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="saving_purchase_id") as STRING ) as saving_purchase_id,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="currency") as STRING ) as currency,
             SUM(CAST((SELECT REPLACE(REPLACE(REPLACE(COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)), ",", "."),"null", "0"), "[object Object]", "0") FROM UNNEST(event_params) WHERE key="value") as FLOAT64) * conversion) as value_eur,
             saving_interest_rate,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="saving_tenor") as STRING ) as saving_tenor,
             ftp,
             user_pseudo_id,

         FROM alex_db_dis_oo as ga

                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.isbd_business_metrics` as bm on CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX", ga.firebase_screen_class_s) = join_key_ga
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.currency_converter` as cc on CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="currency") as STRING )=iso_code
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.target` as target on target_id=CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s)
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.saving_interest_rate` as saving_ir on id_saving_interest=CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)), "ALEX", CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="saving_name") as STRING ))
                  INNER JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.stream_id_prod` as sip on ga.stream_id=sip.stream_id
         where
                 PARSE_DATE('%Y%m%d', event_date) <= DATE_ADD(CURRENT_DATE(), INTERVAL -2 DAY) and
                 ga.firebase_screen_class_s="savings" and event_name = "purchase_saving"
           and (REGEXP_CONTAINS(CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ), r"\binternetbanking.alexbank.com\b") or ga.platform in ("ANDROID", "IOS"))
         group by 1,2,3,4,5,6,7,8,9,10,12,13,14,15),

    alex_db_agg as
        (SELECT
             bank_name,
             event_date,
             event_name,
             fee,
             target,
             country,
             region,
             city,
             count(distinct event_id)  as savings_pieces,
             SUM(value_eur) as savings_volume,
             AVG(ftp) - AVG(saving_interest_rate) as savings_avg_markdown,
             AVG(CAST(SUBSTR(saving_tenor, 1, 2) as INT64)*365 + CAST(SUBSTR(saving_tenor, 3, 2) as INT64)*30 + CAST(SUBSTR(saving_tenor, 5, 2) as INT64)) as days_saving_tenor,
             AVG(saving_interest_rate) as saving_avg_interest,
             COUNT(distinct user_pseudo_id) as savings_active_customer_with_item
         from alex_db_dis
         group by 1,2,3,4,5,6,7,8)

SELECT
    *,
    savings_pieces * fee as savings_income_fees,
    savings_volume * savings_avg_markdown  as savings_income_interest,
    (savings_pieces * fee)  + (savings_volume * savings_avg_markdown)  as savings_total_income,
    savings_volume/savings_pieces as savings_avg_ticket, --viene calcolata in pbi
    ((savings_pieces * fee)  + (savings_volume * savings_avg_markdown)) * 0.059 as savings_income_18_24,
    ((savings_pieces * fee)  + (savings_volume * savings_avg_markdown)) * 0.139 as savings_income_25_34,
    ((savings_pieces * fee)  + (savings_volume * savings_avg_markdown)) * 0.181 as savings_income_35_44,
    ((savings_pieces * fee)  + (savings_volume * savings_avg_markdown)) * 0.163 as savings_income_45_54,
    ((savings_pieces * fee)  + (savings_volume * savings_avg_markdown)) * 0.098 as savings_income_55_64,
    ((savings_pieces * fee)  + (savings_volume * savings_avg_markdown)) * 0.051 as savings_income_over_65,
    ((savings_pieces * fee)  + (savings_volume * savings_avg_markdown)) * 0.309 as savings_income_unknown,
from
    alex_db_agg
order by event_date asc;

insert into `isbd-analytics-global-dwh.isbd_global_ga_PROD.tab_savings_profitability_product_details_tot`
with
    alex_db_dis_o as (
        SELECT *,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ) as page_location,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen_class") as STRING ) as firebase_screen_class,
               CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="firebase_screen") as STRING ) as firebase_screen,
        FROM `digical-boa.analytics_230665710.events_*` as ga --WHERE _TABLE_SUFFIX = DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)

    ),
    alex_db_dis_oo as (
        select  *,
                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_1, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where  platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$')
                      else
                          firebase_screen_class
                    end
                    ) as firebase_screen_class_s,

                (case when platform="WEB" then
                          REGEXP_EXTRACT(
                                  (
                                      SELECT string_agg(url.level_2, ";" ) from `isbd-analytics-global-dwh.isbd_global_ga_PROD.join_url_view` as url where platform=url.regex and STRPOS(REPLACE(REPLACE(REPLACE(page_location, "react/", ""), "domestic/", ""),"//",""), url.url)>0
                                  ), r'([^;]+);?$') else
                          firebase_screen
                    end
                    ) as  firebase_screen_s
        from alex_db_dis_o
    ),

    alex_db_dis as
        (SELECT
             "ALEX" as bank_name,
             PARSE_DATE('%Y%m%d', event_date)as event_date,
             event_name,
             CONCAT(user_pseudo_id, " - ", event_timestamp) as event_id,
             target.target,
             fee,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="saving_purchase_id") as STRING ) as saving_purchase_id,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="currency") as STRING ) as currency,
             SUM(CAST((SELECT REPLACE(REGEXP_REPLACE(REPLACE(COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)), ",", "."),r"not_found|null", "0"), "[object Object]", "0") FROM UNNEST(event_params) WHERE key="value") as FLOAT64)) * AVG(conversion) as value_eur,
             saving_interest_rate,
             CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="saving_tenor") as STRING ) as saving_tenor,
             ftp,
             user_pseudo_id

         FROM alex_db_dis_oo as ga

                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.isbd_business_metrics` as bm on CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s) = join_key_ga
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.currency_converter` as cc on CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="currency") as STRING )=iso_code
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.target` as target on target_id=CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)),"ALEX",ga.firebase_screen_class_s)
                  LEFT JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.saving_interest_rate` as saving_ir on id_saving_interest=CONCAT(EXTRACT(YEAR from PARSE_DATE('%Y%m%d', event_date)), "ALEX", CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="saving_name") as STRING ))
                  INNER JOIN `isbd-analytics-global-dwh.isbd_global_ga_PROD.stream_id_prod` as sip on ga.stream_id=sip.stream_id
         where
                 PARSE_DATE('%Y%m%d', event_date) <= DATE_ADD(CURRENT_DATE(), INTERVAL -2 DAY) and
                 ga.firebase_screen_class_s="savings" and event_name="purchase_saving" --togliere transaction
           and (REGEXP_CONTAINS(CAST((SELECT COALESCE(CAST(value.string_value as string),CAST(value.int_value as string), CAST(value.float_value as string),CAST(value.double_value as string)) FROM UNNEST(event_params) WHERE key="page_location") as STRING ), r"\binternetbanking.alexbank.com\b") or ga.platform in ("ANDROID", "IOS"))
         group by 1,2,3,4,5,6,7,8,10,11,12,13),

    alex_db_agg as
        (SELECT
             bank_name,
             event_date,
             event_name,
             fee,
             target,

             count(distinct event_id)  as savings_pieces,
             SUM(value_eur) as savings_volume,
             AVG(ftp) - AVG(saving_interest_rate) as savings_avg_markdown,
             AVG(CAST(SUBSTR(saving_tenor, 1, 2) as INT64)*365 + CAST(SUBSTR(saving_tenor, 3, 2) as INT64)*30 + CAST(SUBSTR(saving_tenor, 5, 2) as INT64)) as days_saving_tenor,
             AVG(saving_interest_rate) as saving_avg_interest,
             COUNT(distinct user_pseudo_id) as savings_active_customer_with_item
         from alex_db_dis
         group by 1,2,3,4,5)

SELECT
    *,
    savings_pieces * fee as savings_income_fees,
    savings_volume * savings_avg_markdown  as savings_income_interest,
    (savings_pieces * fee)  + (savings_volume * savings_avg_markdown)  as savings_total_income,
    savings_volume/savings_pieces as savings_avg_ticket --viene calcolata in pbi
from
    alex_db_agg
order by event_date asc;
